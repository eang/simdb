/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef SERVER_H
#define SERVER_H

#include <csignal>

#include "net/serverhandler.h"
#include "protocol/serverprotocol.h"
#include "sync.h"
#include "systemhandler.h"

namespace Server
{
    /** Possible server states. */
    enum class State : int
    {
        Default = 0,
        Running = 1,
        Stopped = 2
    };

    extern sig_atomic_t currentServerState;             /** Variable for the signal handler, to signal the listening main thread to exit. */
    const std::string DEFAULT_DATA_DIR = "database";    /** Default directory for the data storage. */
    const std::string DEFAULT_CONF_DIR = "conf";        /** Default directory for the configuration files. */
    const std::string VERSION = "1.0.0";

    struct NetworkThreadArgs;

   /**
    * @brief System main class, implementing the server side.
    * 
    * The Server class implements the main server-side thread; this thread is in charge of the system initialization, the socket listening, the threads handling and the cleanup handling.
    */
    class ServerCore
    {
                
    public:
        
        ServerCore();
        ~ServerCore();

       /**
        * @param argc the number of arguments.
        * #param argv pointer to the arguments vector.
        * @return true if the arguments are valid, false otherwise.
        */
        bool checkArguments(int argc, char **argv);
        
        /** Main function; it starts the listening server after the proper initialization. */
        void start();

    private:
        
        static const Net::Port DEFAULT_PORT = 4444;          /** Default server port. */
        static const int POSIX_DSS_THREAD_TIMER = 30;       /** Scan rate (in seconds) for the new configuration files (POSIX). */  
        static const int WIN32_DSS_THREAD_TIMER = 30000;   /** Scan rate (in milliseconds) for the new configuration files (Win32). */  

        std::string dataDirectory;      /** Data storage directory. */
        std::string confDirectory;      /** Configuration files directory. */
        Net::Port port;                 /** Session server port. */
        int connectionCount;            /** Accepted connections counter during the whole session. */
        
        Mutex mutex;                    /** Mutex for DSS Sequence Numbers update. */
        RWLock lock;                    /** Read/Write lock for the system data consistency. */

        /* System Handlers */
        Net::ServerHandler *serverHandler;              /** Handler of the server side communication. */
        SystemHandler systemHandler;                    /** Handler of the system data. */
        
        /* System core functions */
        
        /** Set the platform-specific signal handler. */
        void setSignalHandler();
        
        /** Create the thread in charge of the new DSS discovering. */
        void createDssThread();
        
       /** 
        * Create the thread in charge of the incoming request.
        * @param connectedSocket The socket of the connection.
        */
        void createNetworkThread(int connectedSocket);
            
        
        /** "Main" function for the DSS thread. */
        void dssThread();

       /**
        * "Main" function for the network threads.
        * @param connectedSocket The socket of the connection.
        * @param threadState State of the thread which handles the connection.
        */
        void networkThread(int connectedSocket, Net::SocketState& threadState);
        

       /** 
        * Process an INSERT command.
        * @param connectedSocket The socket of the connection.
        * @param threadState State of the thread which handles the connection.
        * @param insertRequest The object with the INSERT request. 
        */
        void doInsertCommand(int connectedSocket, Net::SocketState& threadState, const Protocol::InsertRequest& insertRequest);
        
        bool fillSdFields(const Protocol::InsertRequest& insertRequest, Entity::DataSet& dataSet);
        
        bool receiveFiles(int connectedSocket, Net::SocketState& threadState, const Protocol::InsertRequest& insertRequest, Entity::DataSet& dataSet);
        
        bool processFiles(const Protocol::InsertRequest& insertRequest, const Entity::DataSet& dataSet);

        void doRemoveCommand(int connectedSocket, Net::SocketState& threadState, const Protocol::DataSetRequest& dataSetRequest);

        void doGetCommand(int connectedSocket, Net::SocketState& threadState, const Protocol::DataSetRequest& dataSetRequest);
        
        void doSearchCommand(int connectedSocket, Net::SocketState& threadState, const Protocol::SearchRequest& searchRequest);

       /** 
        * Process a SPECLIST command.
        * @param connectedSocket The socket of the connection.
        * @param threadState State of the thread which handles the connection.
        */
        void doSpeclistCommand(int connectedSocket, Net::SocketState& threadState);

        /** Server cleanup. */
        void shutdown();
        
        /** Print the server usage. */
        void printUsage(const char *program);
        
        /** Print the server "presentation" message. */
        void printInitialMessage();
        
        /** Return the "this" pointer of the current Server instance. */
        static ServerCore *getContext();

        /* Platform-specific declarations for the threads starting functions and for the signal handling functions. */
#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))
        
        static unsigned int __stdcall win32DssThread(void *args);       // __stdcall rqeuired by _beginthreadex() 
        static unsigned int __stdcall win32NetworkThread(void *args);   
        static BOOL WINAPI consoleHandler(DWORD signal);

        std::map<unsigned int, HANDLE>  networkThreadHandles;           /** mapping between the running network threads ID and their handles. */
        std::map<unsigned int, NetworkThreadArgs*> networkThreadArgs;   /** mapping between the running network threads ID and their heap arguments. */
        HANDLE dssThreadHandle;                                         /** DSS thread handle. */

        Event dssThreadEvent;                                           /** Event to signal the DSS thread end. */

#else
        static void *posixDssThread(void *args);
        static void *posixNetworkThread(void *args);
        static void signalHandler(int signal);
        
        std::map<pthread_t, NetworkThreadArgs*> networkThreads;
        pthread_t dssThreadId;
        
        CondVarBundle dssThreadCond;        /** COndition to signal the DSS thread end.*/
        
#endif

    };

    /** Struct with the arguments of the DSS thread. */
    struct DssThreadArgs
    {
        ServerCore *server;     /** Server instance. */
    };

    /** Struct with the arguments of the network threads. */
    struct NetworkThreadArgs
    {
        int connectedSocket;            /** Socket of the connection. */
        Net::SocketState threadState;   /** State of the thread. */
        ServerCore *server;                 /** Server instance. */
    };

}




#endif
