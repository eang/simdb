/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef LOGS_H
#define LOGS_H

#include <string>
#include <iostream>

#include "../utils/utils.h"

/** @namespace logs A convenience namespace to simplify the server log. */
namespace Logs
{

   /**
    * @brief A wrapper logger class which automatically writes a service "prompt" to the log messages.
    * Given a certain custom <prompt> string, every message <msg> will be print on output with the following format:
    * $ [timestamp prompt] msg
    * where the timestamp is made by date and hours.
    */
    class Logger
    {

    public:

        Logger(std::ostream& out, const std::string& msg) : outputStream(out), promptMessage(msg) {}

        template <typename T>                           // defined here to support a generic T type
        std::ostream& operator<<(const T& message)
        {
            outputStream << "[" << Utils::getCurrentTime() << " " << promptMessage << "] " << message;
            return outputStream;
        }

    private:

        std::ostream& outputStream;         /** The underlying output stream, usually stdout/stderr through cout/cerr. */
        const std::string promptMessage;    /** A custom message displayed in the prompt. */
    };

    static Logger out = Logger(std::cout, "log");
    static Logger err = Logger(std::cerr, "err");
}

#endif


