/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifdef DEBUG_MODE
#define DEBUG(x) do { std::cerr << "DEBUG: " << x << std::endl; } while (0)
#else
#define DEBUG(x) do { ; } while (0)
#endif

#include <pthread.h>

#include <csignal>

#include "filesystem/filesystem.h"
#include "server/logs.h"
#include "server/servercore.h"
#include "utils/utils.h"

using std::endl;
using std::map;
using std::string;

using Logs::err;
using Logs::out;

using Net::SocketHandler;
using Net::SocketState;

using Server::ServerCore;

void ServerCore::setSignalHandler()
{
    struct sigaction newAction;

    newAction.sa_handler = signalHandler;   // pointer to the handler function
    newAction.sa_flags = 0;                 // cancel all flags, we don't want the flag SA_RESTART because we *want* the accept() to fail with the EINTR error

    if (sigfillset(&newAction.sa_mask) == -1)                           // fill the block mask, we want to block all the other signals during ths signalHandler() execution
        err << Utils::syscallError("sigfillset()", errno) << endl;


    if (sigaction(SIGINT, &newAction, nullptr) == -1)
        err << Utils::syscallError("sigaction()", errno) << endl;

    if (sigaction(SIGTERM, &newAction, nullptr) == -1)
        err << Utils::syscallError("sigaction()", errno) << endl;

    if (sigaction(SIGPIPE, &newAction, nullptr) == -1)
        err << Utils::syscallError("sigaction()", errno) << endl;
}


void ServerCore::createDssThread()
{
    auto dssThreadArgs = new DssThreadArgs;
    dssThreadArgs->server = this;

    int code = pthread_create(&dssThreadId, nullptr, posixDssThread, dssThreadArgs);   // with nullptr the tread can be "joined"

    if (code != 0)
        err << Utils::syscallError("pthread_create()", code) << endl;
}

void ServerCore::createNetworkThread(int connectedSocket)
{
    if (connectedSocket != SocketHandler::GENERIC_SOCKET_ERROR)
    {
        connectionCount++;

        auto networkThreadArgs = new NetworkThreadArgs;
        networkThreadArgs->connectedSocket = connectedSocket;
        networkThreadArgs->threadState = SocketState::Connected;
        networkThreadArgs->server = this;

        pthread_t networkThreadId;

        int code = pthread_create(&networkThreadId, nullptr, posixNetworkThread, networkThreadArgs);

        if (code != 0)
        {
            err << Utils::syscallError("pthread_create()", code) << endl;
        }

        else    // save the thread ID and the pointer to its arguments
        {
            networkThreads.insert({networkThreadId, networkThreadArgs});
        }
    }
}


void *ServerCore::posixNetworkThread(void *args)
{
    auto networkThreadArgs = static_cast<NetworkThreadArgs*>(args);
    auto server = networkThreadArgs->server;

    server->networkThread(networkThreadArgs->connectedSocket, networkThreadArgs->threadState);

    auto id = pthread_self();
    auto threadIt = (server->networkThreads).find(id);

    if (threadIt != (server->networkThreads).end())
    {
        DEBUG("Removing pthread from map");
        (server->networkThreads).erase(threadIt);
    }

    delete networkThreadArgs;
    DEBUG("Freed network thread args memory");

    return nullptr;
}

void *ServerCore::posixDssThread(void *args)
{
    auto dssThreadArgs = static_cast<DssThreadArgs*>(args);
    dssThreadArgs->server->dssThread();

    /* The DSS thread is a "singleton" thread and it can free this memory by itself. */
    delete dssThreadArgs;
    DEBUG("Freed dds thread args memory");

    return nullptr;
}

void ServerCore::signalHandler(int signal)
{
    if (SIGINT == signal || SIGTERM == signal)
    {
        currentServerState = static_cast<int>(State::Stopped);
    }

    /*
     * ... otherwise the signal is SIGPIPE. We capture it just to prevent the server crash.
     * The alternative is the flag MSG_NOSIGNAL on the send(), but it's not working on Windows.
     */
}

void ServerCore::shutdown()
{
    // Step 1, signal the condition to close the DSS thread

    pthread_mutex_lock(&dssThreadCond.mutex);
    dssThreadCond.condition = true;
    pthread_cond_signal(&dssThreadCond.cond);
    pthread_mutex_unlock(&dssThreadCond.mutex);

    DEBUG("joining dss thread...");
    int code = pthread_join(dssThreadId, nullptr);

    if (code != 0)
        err << Utils::syscallError("pthread_join()", code) << endl;

    else DEBUG("dss thread joined successfully");

    // Step 2, close the network threads

    for (auto& threadIt : networkThreads)
    {
        auto args = threadIt.second;

        if (args == nullptr)  // it shouldn't happen, but if it does we are forced to skip the join for this thread
        {
            DEBUG("Skipping thread joining...");
            continue;
        }

        args->threadState = SocketState::Disconnected;

        DEBUG("joining network thread...");
        code = pthread_join(threadIt.first, nullptr);

        if (code != 0)
            err << Utils::syscallError("pthread_join()", code) << endl;

        else DEBUG("network thread joined successfully");
    }


    // Step 3, close the listening socket

    if (serverHandler->stopService())
    {
        out << "Listening socket closed successfully" << endl;
    }

    else
    {
        err << "Cannote close listening socket" << endl;
    }

    out << "Session connection counter is " << connectionCount << endl;
    out << "Server stopped, bye." << endl;
}


void ServerCore::dssThread()
{
    struct timespec absoluteTime;

    try
    {
        out << "Current DSS files: " << systemHandler.initDatabase() << endl;

        /* We exit from the loop when: dssThreadCond.condition == true */

        pthread_mutex_lock(&dssThreadCond.mutex);

        DEBUG("testing exit condition...");
        while (!dssThreadCond.condition)    // while the condition is false, let's find new DSS
        {
            DEBUG("exit condition is false, waiting (but only for 30 sec)...");

            if (clock_gettime(CLOCK_REALTIME, &absoluteTime) != 0)
            {
                err << Utils::syscallError("clock_gettime()", errno);
                err << "Fatal error, aborting DSS thread" << endl;
                break;
            }

            absoluteTime.tv_sec += POSIX_DSS_THREAD_TIMER;  // timer size (in seconds) for the cond_wait

            int code = pthread_cond_timedwait(&dssThreadCond.cond, &dssThreadCond.mutex, &absoluteTime);

            if (code == ETIMEDOUT)
            {
                DEBUG("timer is out, lets working...");

                string found = systemHandler.findNewDSS();

                if (!found.empty())
                {
                    out << "Found new DSS files: " << found << endl;
                }

                else DEBUG("nothing found");

            }

            else if (code == 0)
            {
                DEBUG("exit condition is true, leaving loop...");
            }

            else
            {
                err << Utils::syscallError("pthread_cond_timedwait()", code) << endl;
            }
        }

        pthread_mutex_unlock(&dssThreadCond.mutex);

        DEBUG("End of dss thread loop");
    }
    catch (const Filesystem::Error& e)
    {
        err << "Filesystem error: " << e.what() << endl;
    }
    catch (const ParseException& e)
    {
        err << "DSS configuration file parsing error: " << e.what() << endl;
    }
}




