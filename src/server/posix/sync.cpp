/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include "server/sync.h"
#include "utils/utils.h"

using Server::CondVarBundle;
using Server::Mutex;

Mutex::Mutex()
{

    /* Using nullptr all the attribute are set to default; the type is PTHREAD_MUTEX_DEFAULT, i.e. a "fast" mutex. */
    int code = pthread_mutex_init(&mutex, nullptr);

    if (code != 0)
        Utils::syscallError("pthread_mutex_init()", code);
}

Mutex::~Mutex()
{
    int code = pthread_mutex_destroy(&mutex);

    if (code != 0)
        Utils::syscallError("pthread_mutex_destroy()", code);
}

void Mutex::lock()
{
    pthread_mutex_lock(&mutex);
}

void Mutex::unlock()
{
    pthread_mutex_unlock(&mutex);
}


CondVarBundle::CondVarBundle() : condition(false)
{
    int code = pthread_mutex_init(&mutex, nullptr);

    if (code != 0)
        Utils::syscallError("pthread_mutex_init()", code);

    code = pthread_cond_init(&cond, nullptr);

    if (code != 0)
        Utils::syscallError("pthread_mutex_cond()", code);
}

CondVarBundle::~CondVarBundle()
{
    int code = pthread_mutex_destroy(&mutex);

    if (code != 0)
        Utils::syscallError("pthread_mutex_destroy()", code);

    code = pthread_cond_destroy(&cond);

    if (code != 0)
        Utils::syscallError("pthread_mutex_destroy()", code);
}

