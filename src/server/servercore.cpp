/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifdef DEBUG_MODE
#define DEBUG(x) do { std::cerr << "DEBUG: " << x << std::endl; } while (0)
#else
#define DEBUG(x) do { ; } while (0)
#endif

#include "servercore.h"

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <sstream>

#include <b64/decode.h>
#include <b64/encode.h>

#include "filesystem/filesystem.h"
#include "logs.h"
#include "simdb/simdb.h"
#include "utils/utils.h"

using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;
using std::ios;
using std::ios_base;
using std::istringstream;
using std::logic_error;
using std::map;
using std::ofstream;
using std::ostringstream;
using std::string;
using std::vector;

using Entity::DataItem;
using Entity::DataSet;
using Entity::DataSetSpecifier;
using Entity::SetDescriptor;
using Entity::Tag;
using Entity::UnknownKeyError;
using Entity::WrongTypeError;

using Logs::err;
using Logs::out;

using Protocol::Command;
using Protocol::DataSetRequest;
using Protocol::InsertRequest;
using Protocol::Handler;
using Protocol::SearchRequest;
using Net::SocketState;

using Server::ServerCore;

sig_atomic_t Server::currentServerState = static_cast<int>(State::Default);  // definition required by the extern used in Server.h
ServerCore *context;

ServerCore::ServerCore() :
    dataDirectory {DEFAULT_DATA_DIR},
    confDirectory {DEFAULT_CONF_DIR},
    port {DEFAULT_PORT},
    connectionCount {0},
    serverHandler {nullptr},
    systemHandler {DEFAULT_DATA_DIR, DEFAULT_CONF_DIR}
{
    context = this;
    setSignalHandler();
}

ServerCore::~ServerCore()
{
    context = nullptr;
    delete serverHandler;
    serverHandler = nullptr;
}

bool ServerCore::checkArguments(int argc, char **argv)
{
    if (!argv)
        return false;

    vector<string> arg {argv + 1, argv + argc};      // vector with all the arguments, excluding the first one (i.e. argv+0)
        
    if (find(arg.begin(), arg.end(), "-h") != (arg.end()))
    {
        printUsage(argv[0]);
        return false;
    }

    if (find(arg.begin(), arg.end(), "-v") != (arg.end()))
    {
        cout << "SimDB server: version " << VERSION << endl;
        cout << "Using:" << endl;
        cout << "* SimDB library: version " << Simdb::LIBRARY_VERSION << endl;
        cout << "* SimDB protocol: version " << Simdb::PROTOCOL_VERSION << endl;
        return false;
    }

    auto it = find(arg.begin(), arg.end(), "-d");
    if (it != (arg.end()))
    {
        if ((it + 1) != arg.end())
        {
            dataDirectory = Utils::removeEndSlash(*(++it));     // we don't want the ending slash, it will be added when necessary
            systemHandler.setDataDirectory(dataDirectory);
        }

        else
        {
            cerr << "Empty data directory" << endl;
            printUsage(argv[0]);
            return false;
        }
    }

        
    it = find(arg.begin(), arg.end(), "-c");
            
    if (it != arg.end()) 
    {
        if ((it + 1) != arg.end())
        {
            confDirectory = Utils::removeEndSlash(*(++it));
            systemHandler.setConfDirectory(confDirectory);
        }

        else
        {
            cerr << "Empty conf directory" << endl;
            printUsage(argv[0]);
            return false;
        }
    }
            
    it = find(arg.begin(), arg.end(), "-p");
            
    if (it != arg.end())
    {
        if ((it + 1) != arg.end())
        {
            int n = atoi((++it)->c_str());
            
            if (n < Net::VALID_PORT_NUMBER)
            {
                cerr << *it << " is an invalid port number, insert an integer >= " << Net::VALID_PORT_NUMBER << endl;
                return false;
            }
            
            port = n;
        }
        
        else
        {
            cerr << "Empty port" << endl;
            printUsage(argv[0]);
            return false;
        }
    }

    return true;
}


void ServerCore::start()
{
    serverHandler = new Net::ServerHandler(port);  // only now we can create the ServerHandler object
    currentServerState = static_cast<int>(State::Running);
    
    printInitialMessage();

    if (serverHandler->createSocket())      
    {
        if (serverHandler->startListening())
        {
            createDssThread();  // start the DSS thread

            while (currentServerState == static_cast<int>(State::Running))
            {
                int s = serverHandler->acceptConnection();
                createNetworkThread(s);
                DEBUG("end accept loop iteration");
            }
            
            shutdown();  // cleanup
        }

        else err << "Cannot listen for incoming request, aborting." << endl;
    }

    else err << "Fatal error creating socket, aborting." << endl;
}


void ServerCore::networkThread(int connectedSocket, SocketState& threadState)
{
    string request = serverHandler->receiveMessage(connectedSocket, threadState);
    DEBUG("original ASCII request is: \n" + request);

    try
    {
        auto requestMessage = Handler::parseRequest(request);

        switch (requestMessage->getCommand())
        {
            case Command::Insert:
            {
                auto insertRequest = static_cast<InsertRequest *>(requestMessage);
                out << "Received message: " << *insertRequest << endl;
                out << "Serving INSERT command..." << endl;
                doInsertCommand(connectedSocket, threadState, *insertRequest);
                delete insertRequest;
                break;
            }
            case Command::Remove:
            {
                auto dataSetRequest = static_cast<DataSetRequest *>(requestMessage);
                out << "Received message: " << *dataSetRequest << endl;
                out << "Serving REMOVE command..." << endl;
                doRemoveCommand(connectedSocket, threadState, *dataSetRequest);
                delete dataSetRequest;
                break;
            }
            case Command::Get:
            {
                auto dataSetRequest = static_cast<DataSetRequest *>(requestMessage);
                out << "Received message: " << *dataSetRequest << endl;
                out << "Serving GET command..." << endl;
                doGetCommand(connectedSocket, threadState, *dataSetRequest);
                delete dataSetRequest;
                break;
            }
            case Command::Search:
            {
                auto searchRequest = static_cast<SearchRequest *>(requestMessage);
                out << "Received message: " << *searchRequest << endl;
                out << "Serving SEARCH command..." << endl;
                doSearchCommand(connectedSocket, threadState, *searchRequest);
                delete searchRequest;
                break;
            }
            case Command::Speclist:
                out << "Received message: " << *requestMessage << endl;
                out << "Serving SPECLIST command..." << endl;
                doSpeclistCommand(connectedSocket, threadState);
                delete requestMessage;
                break;
            case Command::Unspecified:
                err << "Invalid command" << endl;
                delete requestMessage;
                break;
        }
    }
    catch (const Protocol::Syntax::Error& e)
    {
        err << "Message Syntax error: " << e.what() << endl;
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::GENERIC_ERROR, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);   
    }
    
    DEBUG("End of network thread");
}

void ServerCore::doInsertCommand(int connectedSocket, SocketState& threadState, const InsertRequest& insertRequest)
{
    if (!systemHandler.holdsDss(insertRequest.getDssName()))   // STEP 1: check DSS name
    {
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::NO_SUCH_SPEC, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
        return;
    }
    
    DataSet dataSet {systemHandler.getDss(insertRequest.getDssName())};
    auto dataItems = insertRequest.getDataItems();

    try
    {
        if (insertRequest.getSdFields().size() > SetDescriptor::MAX_FIELDS_NUMBER || dataItems.size() > DataSetSpecifier::MAX_ITEMS_NUMBER) // STEP 2: check if there are too many fields or items
        {
            serverHandler->sendMessage(connectedSocket, Protocol::Syntax::TOO_MUCH_DATA, threadState);
            serverHandler->closeConnection(connectedSocket, threadState);
            return;
        }
        
        if (!fillSdFields(insertRequest, dataSet))      // STEP 3: fill the SetDescriptor of the new DataSet and check for syntax errors
        {
            serverHandler->sendMessage(connectedSocket, Protocol::Syntax::UNKNOWN_FIELD, threadState);
            serverHandler->closeConnection(connectedSocket, threadState);
            return;
        }
        
        for (auto& item : dataSet.getDataItems())
        {
            vector<DataItem>::const_iterator newItem = find(dataItems.begin(), dataItems.end(), item);

            if (item.getTag() == Tag::Necessary && newItem == dataItems.end())   // NECESSARY item not found in the collection provided by the request
            {
                serverHandler->sendMessage(connectedSocket, Protocol::Syntax::INCOMPLETE_SET, threadState);
                serverHandler->closeConnection(connectedSocket, threadState);
                err << "Missing necessary Data Item <" << item.getName() << ">" << endl;
                return;
            }

            else if (newItem != dataItems.end())   // add the specified attributes to the DataItem of the new DataSet
            {
                item.setFileName(newItem->getFileName());
                item.setOriginalSize(newItem->getOriginalSize());
                item.isPresent(true);
            }
        }

        mutex.lock();               // STEP 5: generate the Sequence Number
        systemHandler.generateSequenceNumber(insertRequest.getDssName(), dataSet);
        mutex.unlock();
        
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::OK, threadState);      // Inital steps passed, send OK to the client to start the file sending
        
        if (!receiveFiles(connectedSocket, threadState, insertRequest, dataSet))    // STEP 6: receive the files
            return;     // the connection is already closed by receiveFiles()
        
        lock.wLock();                                   // STEP 7: use the write lock to update the SD-index
        dataSet.setTimestamp(Utils::getCurrentTime());
        systemHandler.addDataSet(insertRequest.getDssName(), dataSet);
        lock.wUnlock();
        
        if (!processFiles(insertRequest, dataSet))          // STEP 8: check encodingSize and then decode and save the data
        {
            serverHandler->sendMessage(connectedSocket, Protocol::Syntax::GENERIC_ERROR, threadState);
            serverHandler->closeConnection(connectedSocket, threadState);
            return;
        }
        
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::OK + " " + Utils::toString(dataSet.getSetDescriptor().getSequenceNumber()), threadState);      // FINAL STEP: send the sequence number to the client
        
        for (const auto& item : dataSet.getDataItems())
            free(item.getEncoding());

        out << "All files received and stored." << endl;
        serverHandler->closeConnection(connectedSocket, threadState);
    }
    catch (const UnknownKeyError& e)
    {
        err << e.what() << endl;
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::UNKNOWN_NAME, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
    }
    catch (const WrongTypeError& e)
    {
        err << e.what() << endl;
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::WRONG_TYPE, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
    }
    catch (const Filesystem::Error& e)
    {
        err << e.what() << endl;
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::GENERIC_ERROR, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
    }
    catch (const logic_error& e)
    {
        err << e.what() << endl;
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::GENERIC_ERROR, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
    }
}

bool ServerCore::fillSdFields(const InsertRequest& insertRequest, DataSet& dataSet)
{
    auto sdFields = insertRequest.getSdFields();
    auto& setDescriptor = dataSet.getSetDescriptor();          // reference to the SD of the new DataSet
    
    for (const auto& it : sdFields)
    {
        string key = it.first;
        string value = it.second;
        
        if (Utils::isIntegerString(value))
            setDescriptor.addValue(key, atoi(value.c_str()));
        
        else if (Utils::isFloatString(value))
            setDescriptor.addValue(key, (float) atof(value.c_str()));
        
        else if (Utils::isQuoted(value) && !Utils::isQuotedSentence(value)) // it's a word, we can remove the single quotes
            setDescriptor.addValue(key, value.substr(1, value.length() - 2));   

        else if (Utils::isQuotedSentence(value))    // it's a sentence, we keep the quotes for convenience
            setDescriptor.addValue(key, value);
        
        else   // error "8 Unknown field", it's neither an int/float nor a single quoted string
            return false;
    }
    
    return true;
}


bool ServerCore::receiveFiles(int connectedSocket, SocketState& threadState, const InsertRequest& insertRequest, DataSet& dataSet)
{
    auto dataItems = dataSet.getDataItems();
    string itemName;
    size_t encodedSize;

    /* The client might send the files in a different order from the one in the configuration file. 
     * So we use a normal for and then we search the file every time. 
     */
    for (size_t i = 0; i < insertRequest.getDataItems().size(); i++)   // Items number; dataItems.size() is useless here because includes also the unnecessary ones.
    {
        DEBUG("waiting file header...");
        string message = serverHandler->receiveMessage(connectedSocket, threadState);
        
        istringstream parser {message};
        parser >> itemName >> encodedSize;

        auto receivedItem = find(dataItems.begin(), dataItems.end(), DataItem(itemName));

        if (receivedItem != dataItems.end())
        {
            receivedItem->setEncodedSize(encodedSize);

            out << "Receiving file " << receivedItem->getFileName() << " (" << Utils::formatSize(receivedItem->getEncodedSize()) << ")" << endl;
        
            serverHandler->sendMessage(connectedSocket, Protocol::Syntax::FILE_ACK, threadState);   // ACK for the file "header" message
            char *encodedFile = serverHandler->receiveFile(connectedSocket, threadState);
            DEBUG("exit from receiveFile()");
        
            if (encodedFile == nullptr)
            {
                err << "Failed receiving of file " << receivedItem->getFileName() << endl;
            
                serverHandler->sendMessage(connectedSocket, Protocol::Syntax::GENERIC_ERROR, threadState);
                serverHandler->closeConnection(connectedSocket, threadState);
            
                for (const auto& item : dataItems)
                    free(item.getEncoding());
            
                return false;
            }
        
            receivedItem->setEncoding(encodedFile);
            serverHandler->sendMessage(connectedSocket, Protocol::Syntax::OK, threadState);
        }

        else    // at this time all the necessary items has been received 
        {
            err << itemName << " is not a DataItem name" << endl;
            serverHandler->sendMessage(connectedSocket, Protocol::Syntax::TOO_MUCH_DATA, threadState);
            serverHandler->closeConnection(connectedSocket, threadState);
            return false;
        }
    }
    
    return true;
}

bool ServerCore::processFiles(const InsertRequest& insertRequest, const DataSet& dataSet)
{
    const auto& dataItems = dataSet.getDataItems();

    /* We iterate on the items of the request and not on the ones of the DataSet object, 
     * which may also have some unnecessary items not in the request. */
    
    for (const auto &it : insertRequest.getDataItems())
    {
        auto item = find(dataItems.begin(), dataItems.end(), DataItem(it.getName()));

        if (item != dataItems.end())
        {
            string storePath = systemHandler.getStorePath(dataSet.getName(), item->getName(), dataSet.getSequenceNumber(), item->getGroup(), item->getTag());
            Filesystem::createDirectory(storePath);
            storePath += "/" + item->getFileName();
            
            string encoding(item->getEncoding());
            
            if (encoding.length() > item->getEncodedSize())
            {
                err << "Encoding of " << item->getFileName() << " has " << Utils::formatSize(encoding.length()) << " instead of " << Utils::formatSize(item->getEncodedSize()) << endl;

                for (const auto& encIt : dataItems)
                    free(encIt.getEncoding());
                
                return false;
            }
        
            istringstream input {encoding, ios_base::in | ios_base::binary};
            ofstream output {storePath.c_str(), ios_base::out | ios_base::binary};
            
            if (!output.is_open())
            {
                cerr << "Cannot save " << item->getFileName() << " in " << storePath << endl;
                return false;
            }

            base64::decoder dec;
            dec.decode(input, output);
            
            output.close();

            out << item->getFileName() << " decoded and saved (" << Utils::formatSize(Filesystem::getFileSize(storePath)) << ")" << endl;
        }
    }

    return true;
}


void ServerCore::doRemoveCommand(int connectedSocket, SocketState& threadState, const DataSetRequest& dataSetRequest)
{
    if (!systemHandler.holdsDss(dataSetRequest.getDssName()))   
    {
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::NO_SUCH_SPEC, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
        return;
    }

    lock.wLock();
    bool removed = systemHandler.removeDataSet(dataSetRequest.getDssName(), dataSetRequest.getSequenceNumber());
    lock.wUnlock();

    if (!removed)
    {
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::NO_SUCH_SET, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
        return;
    }

    string path = systemHandler.getStorePath(dataSetRequest.getDssName(), dataSetRequest.getSequenceNumber());

    try
    {
        Filesystem::removeDirectory(path);

        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::OK, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);

        out << "DataSet " << dataSetRequest.getSequenceNumber() << " of " << dataSetRequest.getDssName() << " removed successfully" << endl;
    }
    catch (const Filesystem::Error& e)
    {
        err << e.what() << endl;
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::GENERIC_ERROR, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
    }
}


void ServerCore::doGetCommand(int connectedSocket, SocketState& threadState, const DataSetRequest& dataSetRequest)
{
    if (!systemHandler.holdsDss(dataSetRequest.getDssName()))   // STEP 1: check DSS name
    {
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::NO_SUCH_SPEC, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
        return;
    }

    lock.rLock();           // STEP 2: check if the sequence number is valid
    bool found = systemHandler.holdsDataSet(dataSetRequest.getDssName(), dataSetRequest.getSequenceNumber());
    lock.rUnlock();
    
    if (!found)
    {
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::NO_SUCH_SET, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
        return;
    }
    
    lock.rLock();       // STEP 3: get a COPY of the requested DataSet (because during the download another client might edit that DataSet)
    DataSet dataSet = systemHandler.getDataSet(dataSetRequest.getDssName(), dataSetRequest.getSequenceNumber());
    lock.rUnlock();
    
    auto& setDescriptor = dataSet.getSetDescriptor();
    map<string, string> sdFields;
    map<string, string> dataItems;
    
    for (const auto& it : setDescriptor.getStringFields())
    {
        sdFields.insert({it.first, it.second});
    }
    
    for (const auto& it : setDescriptor.getIntFields())
    {
        sdFields.insert({it.first, Utils::toString(it.second)});
    }
    
    for (const auto& it : setDescriptor.getFloatFields())
    {
        sdFields.insert({it.first, Utils::toString(it.second)});
    }   
        
    for (const auto& item : dataSet.getDataItems())
    {
        if (item.isPresent())
            dataItems.insert({item.getName(), item.getFileName()});
    }
    
    string risp = Handler::createGetResponse(dataSet.getSequenceNumber(), sdFields, dataItems);
    DEBUG(risp);
    serverHandler->sendMessage(connectedSocket, risp, threadState);

    base64::encoder enc;

    string ack = serverHandler->receiveMessage(connectedSocket, threadState);   // receive the first ACK, to start the file sending

    if (ack != Protocol::Syntax::FILE_ACK)
    {
        err << "Client not ready for file exchange, aborting..." << endl;
        serverHandler->closeConnection(connectedSocket, threadState);
        return;
    }

    for (const auto& item : dataSet.getDataItems())
    {
        if (item.isPresent())
        {
            string path = systemHandler.getStorePath(dataSetRequest.getDssName(), item.getName(), dataSetRequest.getSequenceNumber(), item.getGroup(), item.getTag());
            path += "/" + item.getFileName();

            ifstream input {path.c_str(), ios::in | ios::binary};
            ostringstream output;

            enc.encode(input, output);
            string encodedFile = output.str();  // may be a huge string...

            serverHandler->sendMessage(connectedSocket, item.getName() + ' ' + Utils::toString(encodedFile.length()) + '\n', threadState);       // send item name and encoded size

            ack = serverHandler->receiveMessage(connectedSocket, threadState);      // receive the ACK for the "header", to continue the file sending
            
            if (ack != Protocol::Syntax::FILE_ACK)
            {
                err << "Client not ready for file exchange, aborting..." << endl;
                serverHandler->closeConnection(connectedSocket, threadState);
                return;
            }

            out << "Sending file " << item.getFileName() << " (encoded size: " << Utils::formatSize(item.getEncodedSize()) << ")..." << endl;

            serverHandler->sendMessage(connectedSocket, encodedFile, threadState);
            input.close();
            
            ack = serverHandler->receiveMessage(connectedSocket, threadState);      // receive the ACK for the file
            
            if (ack != Protocol::Syntax::FILE_ACK)
            {
                err << "Client not ready for file exchange, aborting..." << endl;
                serverHandler->closeConnection(connectedSocket, threadState);
                return;
            }
        }
    }

    out << "All files sent successfully" << endl;
    serverHandler->closeConnection(connectedSocket, threadState);
}

void ServerCore::doSearchCommand(int connectedSocket, SocketState& threadState, const SearchRequest& searchRequest)
{
    if (!systemHandler.holdsDss(searchRequest.getDssName()))   
    {
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::NO_SUCH_SPEC, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
        return;
    }

    auto dss = systemHandler.getDss(searchRequest.getDssName());
    auto setDescriptor = dss.getSetDescriptor();

    for (const auto& it : searchRequest.getQueryFields())
    {
        if (!setDescriptor.existKey(it.first))   // at least one key is not in DSS SetDescriptor
        {
            serverHandler->sendMessage(connectedSocket, Protocol::Syntax::UNKNOWN_FIELD, threadState);
            serverHandler->closeConnection(connectedSocket, threadState);
            return;
        }
    }   

    vector<SetDescriptor> descriptorList;
    
    lock.rLock();
    systemHandler.search(searchRequest.getDssName(), searchRequest.getQueryFields(), descriptorList);
    lock.rUnlock();
    
    string searchResponse = Handler::createSearchResponse(descriptorList);
    DEBUG(searchResponse);
    
    serverHandler->sendMessage(connectedSocket, searchResponse, threadState);
    serverHandler->closeConnection(connectedSocket, threadState);
}


void ServerCore::doSpeclistCommand(int connectedSocket, SocketState& threadState)
{
    if (systemHandler.empty())
    {
        serverHandler->sendMessage(connectedSocket, Protocol::Syntax::GENERIC_ERROR + " Found 0 DSS entries", threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
    }
    
    else
    {
        vector<string> dssList;
        systemHandler.fillDssList(dssList);
        
        string response = Protocol::Syntax::OK + " " + Protocol::Syntax::FOUND_MARKER + " " + Utils::toString(dssList.size());   // "OK 0 FOUND n"
        
        for (const auto& it : dssList)
        {
            response += " " + it;
        }
        
        serverHandler->sendMessage(connectedSocket, response, threadState);
        serverHandler->closeConnection(connectedSocket, threadState);
    }
}   


void ServerCore::printUsage(const char *program)
{
    cout << "Usage: " << program << " [-c conf] [-p port] [-h]" << endl << "Options:" << endl;
    cout << "  -d dbdir\t: dbdir will be the well-know directory for system data storing (default one is ./database)" << endl;  
    cout << "  -c conf\t: conf will be the well-know directory for DSS configuration files (default one is ./conf)" << endl;
    cout << "  -p port\t: port will be the default port for the TCP connection (default one is 4444)" << endl;
    cout << "  -v\t\t: prints the server version number and then exits" << endl;
    cout << "  -h\t\t: prints this help message and then exits" << endl;
}

void ServerCore::printInitialMessage()
{
    out << "SimDB Server started" << endl;
    
    string cwd = Filesystem::getCurrentDirectory();  
    
    /* Check if the directory are absolute paths. If not, the CWD is prefixed. */
    
    if (Filesystem::isAbsolutePath(dataDirectory))  
        out << "System Database directory is: " << dataDirectory  << endl;
    
    else
        out << "System Database directory is: " << cwd + "/" + dataDirectory  << endl;   
    
    if (Filesystem::isAbsolutePath(confDirectory))
        out << "Reading DSS configuration files from directory: " << confDirectory << endl;
        
    else
        out << "Reading DSS configuration files from directory: " << cwd + "/" + confDirectory << endl;   
    
    out << "Listening for connection on port <" << port << "> ..." << endl;
}


ServerCore* ServerCore::getContext()
{
    return context;
}

