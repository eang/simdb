/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef SYNC_H
#define SYNC_H

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))
#include <windows.h>
#else
#include <pthread.h>
#endif


namespace Server
{
    /**
     * @brief A Mutex object.
     *
     * This class implements a platform-indipendent lock mechanism, useful to synchronize threads which share global resources.
     */
    class Mutex
    {
    public:

        Mutex();
        ~Mutex();

        /** Request the mutex control and stop the calling thread if another thread owns the mutex. */
        void lock();

        /** Release the mutex control. */
        void unlock();

    private:

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))
        HANDLE mutex;
#else
        pthread_mutex_t mutex;
#endif
    };


    /**
     * @brief A Read-Write lock object.
     *
     * Simple implementation of the Readers-Writers problem, with priority to readers.
     * A number of readers can obtain the rlock at the same time, while only one writer can obtain the wlock, and only if no writer has a wlock.
     */
    class RWLock
    {

    public:

        RWLock();

        /** Require the read-lock and stop the calling thread if another thread owns a write-lock. */
        void rLock();

        /** Release the read-lock and if the calling thread is the last reader unlock the writers (if any). */
        void rUnlock();

        /** Require the write-lock and stop the calling thread if another thread owns a read or a write lock. */
        void wLock();

        /** Release the write-lock and allow the access to other threads. */
        void wUnlock();

    private:

        int readerCount;        /** Number of writer threads with the read-lock. */
        Mutex countMutex;       /** Mutex used for update the readerCount. */
        Mutex writeMutex;       /** Mutex used for writing, since only one writer is allowed. */

    };


#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

   /**
    * This struct simplifies the usage of a Win32 Event.
    */
    struct Event
    {
        Event();

        ~Event();

        HANDLE event;
    };


#else

   /**
    * This struct simplifies the usage of conditional variables.
    * It bundles the pthread_cond_t variable, a pthread_mutex_t and a boolean representing the condition to wait/signal.
    */
    struct CondVarBundle
    {
        /** Initialize to false the condition and the other variables. */
        CondVarBundle();

        /** Cleanup for the variables. */
        ~CondVarBundle();

        pthread_mutex_t mutex;
        pthread_cond_t cond;
        bool condition;
    };

#endif

}

#endif
