/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef SYSTEM_HANDLER_H
#define SYSTEM_HANDLER_H

#include <map>
#include <stdexcept>
#include <string>
#include <vector>

#include "entity/dataset.h"
#include "entity/datasetspecifier.h"

namespace Server
{

    class SystemHandler
    {

    public:

        SystemHandler(const std::string &dataDir, const std::string &confDir);

       /**
        * @param dataDir The directry for the files storage.
        */
        void setDataDirectory(const std::string &datatDir);

       /**
        * @param confDir The directory with the configuration file.
        */
        void setConfDirectory(const std::string &confDir);

       /**
        * Initalize the system database looking for configuration files to be parsed into DataSetSpecifier objects.
        * @return A string with the found DSS names.
        */
        std::string initDatabase();

        /**
        * Look for new configuration files added later in the system conf directory.
        * For each new entry found a new DataSetSpecifier object is added to the system.
        * @return A string with the found DSS names or an empty string if no DSS was found.
        */
        std::string findNewDSS();

       /**
        * @param dssList String vector to store the current DSS names within the system.
        */
        void fillDssList(std::vector<std::string> &dssList);

       /**
        * @return true if the system has no DSS, false otherwise.
        */
        bool empty();

       /**
        * @param dss A DSS name.
        * @return true if the given DSS exists in the system, false otherwise.
        */
        bool holdsDss(const std::string &dss);

       /**
        * @param dssName A DSS name.
        * @param sequenceNumber The sequence number of a DataSet instance of the given DSS.
        * @return true if the specified DataSet exists in the system, false otherwise.
        */
        bool holdsDataSet(const std::string& dssName, int sequenceNumber);

        Entity::DataSetSpecifier &getDss(const std::string &dssName);

        void generateSequenceNumber(const std::string &dssName, Entity::DataSet &dataSet);

       /**
        * Add a new DataSet to the system collection.
        * @param dssName The name of a DSS.
        * @param dataSet The DataSet instance provided by the client.
        */
        void addDataSet(const std::string &dssName, Entity::DataSet &dataSet);

       /**
        * Build the extended path of the specified DataItem.
        * @param dssName The DSS of the DataItem.
        * @param itemName The DataItem name.
        * @param sequenceNumber The Sequence Number of the DataSet where the DataItem belongs.
        * @param group The DataItem group.
        * @param tag The DataItem tag.
        * @return The extended path of the DataItem, built according to specified arguments.
        */
        std::string getStorePath(const std::string &dssName, const std::string &itemName, int sequenceNumber, Entity::Group group, Entity::Tag tag);

       /**
        * Build the path of the DataSet with the number <sequenceNumber> of the <dssName> DSS.
        * @param dssName The DSS instantiated by the DataSet.
        * @param sequenceNumber The sequence number of the specified DataSet.
        * @return Extended path of the specified DataSet.
        */
        std::string getStorePath(const std::string &dssName, int sequenceNumber);

       /**
        * Remove the specified DataSet and update the DSS SD-Index file.
        * @param dssName The DSS instantiated by the DataSet.
        * @param sequenceNumber The sequence number of the specified DataSet.
        * @return true if the operation succeeds, false otherwise.
        */
        bool removeDataSet(const std::string &dssName, int sequenceNumber);

       /**
        * Get a copy of the specified DataSet.
        * @param dssName The DSS instantiated by the DataSet.
        * @param sequenceNumber The sequence number of the specified DataSet.
        * @return The request DataSet object.
        * @throw logic_error if the specified DataSet has not been found.
        */
        Entity::DataSet getDataSet(const std::string &dssName, int sequenceNumber);

       /**
        * Execute a search operation on the system data.
        * @param dssName The name of a DSS.
        * @param queryFields The SetDescriptor fields specified as query fields.
        * @param descriptorList SetDescriptors vector to be stored with the matching SetDescriptors (if any).
        */
        void search(const std::string &dssName, const std::map<std::string, std::string> &queryFields, std::vector<Entity::SetDescriptor> &descriptorList);

    private:

        std::string dataDirectory;
        std::string confDirectory;
        std::map<std::string, Entity::DataSetSpecifier> dssArray;                       /** System DSS collection. */
        std::map<std::string, std::vector<Entity::DataSet> > dataSetCollection;         /** System DSS instances. */
        std::map<std::string, int> dssSequenceNumbers;                                  /** Collection of assigned sequence numbers. */

       /**
        * Parse a DSS configuration file into a DataSetSpecifier object.
        * @param conf Configuration file name, i.e. file.conf.
        * @throw parseException Exception thrown to specify where a (syntax) error has been found.
        * @return true if the parsing succeeds, false otherwise.
        */
        bool parse(const std::string &conf);

       /**
        * Add keys to a SetDescriptor, based on their type.
        * @throw parseException Exception thrown to specify where a (syntax) error has been found.
        */
        void addSetDescriptorField(const std::string &name, const std::string &type, Entity::DataSetSpecifier &dss);

       /**
        *  Add DataItem objects to the DSS.
        * @throw parseException Exception thrown to specify where a (syntax) error has been found.
        */
        void addDataItem(const std::string &name, const std::string &tag, const std::string &group, Entity::DataSetSpecifier &dss);

       /**
        * Create an empty SD-index file.
        * @param path The path of the SD-index.
        */
        void createIndex(const std::string &path);

       /**
        * Update the SD-index with a new DataSet info.
        * @param path The path of the SD-index.
        * @param dataSet A new DataSet object.
        */
        void updateIndex(const std::string &path, const Entity::DataSet &dataSet);

       /**
        * Read the SD-index of the specified DSS and udpate the sequence numbers map.
        * @param dssName The DSS directory name.
        */
        void initFromIndex(const std::string &dssName);

        /** Initialize the system DataSet collection with the data from the old system sessions. */
        void fillDatSetCollection();
    };

   /**
    * Exception thrown to specify where a (syntax) error has been found while parsing a configuration file.
    */
    class ParseException : public std::runtime_error
    {
        public:
            ParseException(const std::string &msg) : std::runtime_error(msg) {}
    };
}

#endif
