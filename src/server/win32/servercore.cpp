/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifdef DEBUG_MODE
#define DEBUG(x) do { std::cerr << "DEBUG: " << x << std::endl; } while (0)
#else
#define DEBUG(x) do { ; } while (0)
#endif

// This includes winsock2.h, which has to be included before windows.h
#include "server/servercore.h"

#include <windows.h>
#include <process.h>   // required by _beginthreadex()

#include "filesystem/filesystem.h"
#include "server/logs.h"
#include "utils/utils.h"

using std::endl;
using std::string;

using Logs::err;
using Logs::out;

using Net::SocketHandler;
using Net::SocketState;

using Server::ServerCore;

void ServerCore::setSignalHandler()
{   
    if (!SetConsoleCtrlHandler(consoleHandler, TRUE))
    {
        err << Utils::syscallError("SetConsoleCtrHandler()", GetLastError()) << endl;
    }
}

void ServerCore::createDssThread()
{
    auto dssThreadArgs = new DssThreadArgs;
    dssThreadArgs->server = this;
    
    dssThreadHandle = (HANDLE) _beginthreadex(nullptr, 0, win32DssThread, dssThreadArgs, 0, nullptr);

    if (!dssThreadHandle)
    {
        err << Utils::syscallError("_beginthreadex()", GetLastError()) << endl;
    }
}

void ServerCore::createNetworkThread(int connectedSocket)
{
    if (connectedSocket != SocketHandler::GENERIC_SOCKET_ERROR)
    {
        connectionCount++;
        
        auto threadArgs = new NetworkThreadArgs; 
        threadArgs->connectedSocket = connectedSocket;
        threadArgs->threadState = SocketState::Connected;
        threadArgs->server = this;
        
        unsigned int threadId;
        auto threadHandle = (HANDLE) _beginthreadex(nullptr, 0, win32NetworkThread, threadArgs, 0, &threadId);

        if (!threadHandle)
        {
            err << Utils::syscallError("_beginthreadex()", GetLastError()) << endl;
        }

        else  // save the handle and the pointer to the arguments
        {
            networkThreadHandles.insert({threadId, threadHandle});
            networkThreadArgs.insert({threadId, threadArgs});
        }
    }
}


BOOL ServerCore::consoleHandler(DWORD signal)
{
    switch(signal) 
    { 
        case CTRL_C_EVENT: 
        case CTRL_BREAK_EVENT: 
        case CTRL_CLOSE_EVENT: 
        case CTRL_LOGOFF_EVENT:     
        case CTRL_SHUTDOWN_EVENT: 
            currentServerState = static_cast<int>(State::Stopped);
            getContext()->serverHandler->stopService();     // it must be done here, since in Win32 this is a new thread and the main thread is still on the accept()
            return TRUE;
        default: 
			return FALSE;
    } 
}

unsigned int __stdcall ServerCore::win32NetworkThread(void *args)
{
    auto threadArgs = (NetworkThreadArgs *) args;
    auto server = threadArgs->server;

    server->networkThread(threadArgs->connectedSocket, threadArgs->threadState);

    auto id = GetCurrentThreadId();
    auto threadIt = (server->networkThreadArgs).find(id);

    if (threadIt != (server->networkThreadArgs).end())
    {
        DEBUG("Found thread id in map, removing...");
        (server->networkThreadArgs).erase(threadIt);
    }

    delete threadArgs;
    DEBUG("Freed network thread args memory");

    return 0;
}

unsigned int __stdcall ServerCore::win32DssThread(void *args)
{
    auto dssThreadArgs = (DssThreadArgs *) args;
    dssThreadArgs->server->dssThread();

    delete dssThreadArgs;
    DEBUG("Freed dss thread args memory");

    return 0;
}


void ServerCore::shutdown()
{
    // Step 1, signal the event to close the DSS thread
    
    DEBUG("Segnaling event for dss thread...");
    
    if (!SetEvent(dssThreadEvent.event))
    {
        err << Utils::syscallError("SetEvent()", GetLastError()) << endl;
    }

    auto code = WaitForSingleObject(dssThreadHandle, INFINITE);

    if (code == WAIT_FAILED)
    {
        err << Utils::syscallError("WaitForSingleObject() on mutex", GetLastError()) << endl;
    }

    else if (code == WAIT_OBJECT_0)
    {
        DEBUG("dss thread is signaled");
        if (!CloseHandle(dssThreadHandle))
        {
            err << Utils::syscallError("CloseHandle() on dss thread", GetLastError()) << endl;
        }

        else DEBUG("Dss thread handle closed");
    }
    
    // Step 2, close the network threads
    
	for (auto& threadIt : networkThreadArgs)
	{
        auto id = threadIt.first;
        auto args = threadIt.second;

        if (!args)  // it shouldn't happen, but if it does we are forced to skip the wait for this thread
        {
            DEBUG("Skipping thread Wait()-ing...");
            continue;
        }

        DEBUG("Waiting downloading thread...");
        args->threadState = SocketState::Disconnected;

        code = WaitForSingleObject(networkThreadHandles[id], INFINITE);     // use the ID as the key of the handle

        if (code == WAIT_FAILED)
        {
            err << Utils::syscallError("WaitForSingleObject() on mutex", GetLastError()) << endl;
        }
        
        else if (code == WAIT_OBJECT_0)
        {
            DEBUG("dss thread is signaled");
            if (!CloseHandle(networkThreadHandles[id]))
            {
                err << Utils::syscallError("CloseHandle() on network thread", GetLastError()) << endl;
            }

            else DEBUG("Network thread handle closed");
        }
    }

    out << "Session connection counter is " << connectionCount << endl;
    out << "Server stopped." << endl;

}


void ServerCore::dssThread()
{
    try
    {
        out << "Current DSS files: " << systemHandler.initDatabase() << endl;
        
        while (true)    // when the event is signaled, we exit from the loop
        {
            DEBUG("Timed wait for the event (30 sec)...");
            
            auto code = WaitForSingleObject(dssThreadEvent.event, WIN32_DSS_THREAD_TIMER);
            
            if (code == WAIT_TIMEOUT)
            {
                DEBUG("timer is out, lets working...");
                
                string found = systemHandler.findNewDSS();
                
                if (!found.empty())
                {
                    out << "Found new DSS files: " << found << endl;
                }               
            }
            
            else if (code == WAIT_OBJECT_0)     // event signaled
                break;
        }

        DEBUG("End dss thread loop");
    }
    catch (const Filesystem::Error& e)
    {
        err << "Filesystem error: " << e.what() << endl;
    }
    catch (const ParseException& e)  
    {
        err << "DSS configuration file parsing error: " << e.what() << endl;
    }
}
