/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <iostream>

#include "server/logs.h"
#include "server/sync.h"
#include "utils/utils.h"

using std::endl;

using Logs::err;

using Server::Event;
using Server::Mutex;

Mutex::Mutex()
{
    mutex = CreateMutex(nullptr, false, nullptr);

    if (!mutex)
    {
        err << Utils::syscallError("CreateMutex()", GetLastError()) << endl;
    }
}

Mutex::~Mutex()
{
    if (!CloseHandle(mutex))
    {
        err << Utils::syscallError("CloseHandle() on mutex", GetLastError()) << endl;
    }
}

void Mutex::lock()
{
    auto code = WaitForSingleObject(mutex, INFINITE);

    if (code == WAIT_FAILED)
    {
        err << Utils::syscallError("WaitForSingleObject() on mutex", GetLastError()) << endl;
    }
}

void Mutex::unlock()
{
    if (!ReleaseMutex(mutex))
    {
        err << Utils::syscallError("ReleaseMutex()", GetLastError()) << endl;
    }
}


Event::Event()
{
    event = CreateEvent(nullptr, false, false, nullptr);  // Create a non-signaled auto-resetting event

    if (!event)
    {
        err << Utils::syscallError("CreateEvent()", GetLastError()) << endl;
    }
}

Event::~Event()
{
    if (!CloseHandle(event))
    {
        err << Utils::syscallError("CloseHandle() on event", GetLastError()) << endl;
    }
}






