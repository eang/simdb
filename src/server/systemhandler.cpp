/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <cstdlib>

#include "filesystem/filesystem.h"
#include "logs.h"
#include "protocol/protocol.h"
#include "systemhandler.h"
#include "utils/utils.h"

using std::endl;
using std::ifstream;
using std::ios;
using std::istringstream;
using std::logic_error;
using std::map;
using std::ofstream;
using std::runtime_error;
using std::string;
using std::stringstream;
using std::vector;

using Entity::DataItem;
using Entity::DataSet;
using Entity::DataSetSpecifier;
using Entity::Group;
using Entity::SetDescriptor;
using Entity::Tag;

using Logs::err;

using Server::SystemHandler;

namespace
{
    /* Configuration files syntax strings. */
    
    const string NECESSARY_TAG = "N";
    const string UNNECESSARY_TAG = "U";
    const string INPUT_GROUP = "input";
    const string OUTPUT_GROUP = "output";
    const string STRING_TYPE = "string";
    const string INTEGER_TYPE = "int";
    const string REAL_TYPE = "float";
    
    const string DATASET_DIR = "DataSet_";           /** Default name fot the DataSets directory, prefixed to their Sequence Numbers. */
    const string SD_INDEX = "SD-index";         /** SD-index files name for each DSS directory. */
    const string INPUT_DIR = "Input";           /** Name of the root directory of the input tree. */
    const string OUTPUT_DIR = "Output";         /** Name of the root directory of the output tree. */
    const string NECESSARY_PREFIX = "N_";       /** Prefix for the necessary DataItems directories. */
    const string UNNECESSARY_PREFIX = "U_";     /** Prefix for the unnecessary DataItems directories. */
    const string DESCR_FILE = "Descr";
    
}


SystemHandler::SystemHandler(const string &dataDir, const string &confDir) :
    dataDirectory {dataDir},
    confDirectory {confDir}
{}

void SystemHandler::setDataDirectory(const string& datatDir)
{
    dataDirectory = datatDir;
}


void SystemHandler::setConfDirectory(const string &confDir)
{
    confDirectory = confDir;
}


string SystemHandler::initDatabase()
{
    string dssList;
    vector<string> entries;
    
    if (!Filesystem::existDirectory(confDirectory))
        Filesystem::createDirectory(confDirectory);
    
    if (!Filesystem::existDirectory(dataDirectory))
        Filesystem::createDirectory(dataDirectory);
    
    Filesystem::listDirectory(confDirectory, entries);
    
    if (entries.size() == 0)
    {
        return "<no DSS found>";;
    }
    
    for (const auto& entry : entries)
    {
        if (!parse(entry))
            err << "Failed to open configuration file " << entry << endl;
        
        else
        {
            string dssName = Utils::removeSubstring(entry, ".conf");
            string indexPath = dataDirectory + "/" + dssName + "/" + SD_INDEX;

            dssList += "<" + dssName + "> ";
            

            
            if (!Filesystem::existDirectory(dataDirectory, dssName))
            {
                Filesystem::createDirectory(dataDirectory, dssName);
                createIndex(indexPath);  
            }
            
            else if (Filesystem::existFile(indexPath))
                initFromIndex(dssName);     // read the SD-index and initialize the sequence number
        }
    }
    
    if (dssArray.empty())
        return "<no DSS found>";
    
    fillDatSetCollection();   // initialize the DataSet collection with the data from the old sessions.

    return dssList;
}


string SystemHandler::findNewDSS()
{
    string newDssList;

    size_t oldSize = dssArray.size();
    vector<string> entries;
    
    if (!Filesystem::existDirectory(dataDirectory))
        Filesystem::createDirectory(dataDirectory);
    
    if (!Filesystem::existDirectory(confDirectory))
        Filesystem::createDirectory(confDirectory);
    
    Filesystem::listDirectory(confDirectory, entries);
    
    for (const auto& entry : entries)
    {
        if (dssArray.find(entry) == dssArray.end())
        {
            if (!parse(entry))
                err << "Failed to open configuration file " << entry << endl;
            
            else
            {
                string dssName = Utils::removeSubstring(entry, ".conf");
                string indexPath = dataDirectory + "/" + dssName + "/" + SD_INDEX;
                newDssList += "<" + dssName + "> ";
                
                if (!Filesystem::existDirectory(dataDirectory, dssName))
                {
                    Filesystem::createDirectory(dataDirectory, dssName);
                    createIndex(indexPath);  
                }
            }
        }
    }

    if (oldSize != dssArray.size())         // check if new DSS has been found.
        return newDssList;
    
    return "";
}

void SystemHandler::fillDssList(vector< string > &dssList)
{
    if (!dssArray.empty())
    {
        for (auto& it : dssArray)
        {
            dssList.push_back(it.first);
        }
    }
}


bool SystemHandler::empty()
{
    return dssArray.empty();
}

bool SystemHandler::holdsDss(const string& dss)
{
    return dssArray.find(dss) != dssArray.end();
}

bool SystemHandler::holdsDataSet(const string& dssName, int sequenceNumber)
{
    for (const auto& dataSet : dataSetCollection[dssName])
    {
        if (dataSet.getSequenceNumber() == sequenceNumber)
            return true;
    }
    
    return false;
}

DataSetSpecifier& SystemHandler::getDss(const string& dssName)
{
    return dssArray[dssName];  
}


void SystemHandler::generateSequenceNumber(const string &dssName, DataSet &dataSet)
{
    int sequenceNumber = dssSequenceNumbers[dssName] + 1;
    dataSet.setSequenceNumber(sequenceNumber);
    dssSequenceNumbers[dssName] = sequenceNumber;
}


void SystemHandler::addDataSet(const string &dssName, DataSet &dataSet)
{
    dataSetCollection[dssName].push_back(dataSet);
    
    string dssDir = dataDirectory + "/" + dssName;
    string dssIndex = dssDir + "/" + SD_INDEX;
    string dataSetDir = DATASET_DIR + Utils::toString(dataSet.getSequenceNumber());
    
    if (!Filesystem::existDirectory(dssDir, dataSetDir))
    {
        Filesystem::createDirectory(dssDir, dataSetDir);
        Filesystem::createDirectory(dssDir + "/" + dataSetDir + "/" + INPUT_DIR);
        Filesystem::createDirectory(dssDir + "/" + dataSetDir + "/" + OUTPUT_DIR);
        
        string descrPath = dssDir + "/" + dataSetDir + "/" + DESCR_FILE;   // Create the DataSet Descr file
        ofstream ofs {descrPath.c_str(), ios::out};
        
        //ofs << dataSet.getSetDescriptor() << endl;
        ofs << dataSet;
        ofs.close();
    }
    
    updateIndex(dssIndex, dataSet);
}

string SystemHandler::getStorePath(const string &dssName, const string &itemName, int sequenceNumber, Group group, Tag tag)
{
    string dataSetDir = DATASET_DIR + Utils::toString(sequenceNumber);
    string path = dataDirectory + "/" + dssName + "/" + dataSetDir;
    
    switch (group)
    {
        case Group::Input:
            path += "/" + INPUT_DIR;
            break;
            
        case Group::Output:
            path += "/" + OUTPUT_DIR;
            break;
            
        case Group::Unspecified:
            path = "";
            break;
    }
    
    switch (tag)
    {
        case Tag::Necessary:
            path += "/" + NECESSARY_PREFIX + itemName;
            break;
            
        case Tag::Unnecessary:
            path += "/" + UNNECESSARY_PREFIX + itemName;
            break;
            
        case Tag::Unspecified:
            path = "";
            break;  
    }
    
    return path;   
}

string SystemHandler::getStorePath(const string &dssName, int sequenceNumber)
{
    return dataDirectory + "/" + dssName + "/" + DATASET_DIR + Utils::toString(sequenceNumber);
}

bool SystemHandler::removeDataSet(const string &dssName, int sequenceNumber)
{
    if (sequenceNumber > dssSequenceNumbers[dssName])
        return false;       // Invalid SN 
    
    if (sequenceNumber == dssSequenceNumbers[dssName])
        dssSequenceNumbers[dssName]--;      // If the removed SN is latest one, we want to re-use it at the next INSERT.

    string indexPath = dataDirectory + "/" + dssName + "/" + SD_INDEX;
    vector<string> oldLines;
    string buffer;
    bool found = false;     // whether the specified SN has been found

    ifstream input {indexPath.c_str(), ios::in};

    if (input.is_open())
    {
        stringstream parser;
        int sn;
        
        while (getline(input, buffer))
        {
            parser.str(buffer);
            parser >> sn;

            if (sn == sequenceNumber)
                found = true;
            else
                oldLines.push_back(buffer);
        }
    }
    
    input.close();
    ofstream output {indexPath.c_str(), ios::out};

    if (output.is_open())
    {   
        for (const auto& line : oldLines)
        {
            output << line << endl;
        }
    }

    output.close();

    if (found)   // remove the DataSet
    {
        auto dataSetIt = dataSetCollection[dssName].begin();
        
        while (dataSetIt != dataSetCollection[dssName].end())
        {
            if (dataSetIt->getSequenceNumber() == sequenceNumber)
            {
                dataSetCollection[dssName].erase(dataSetIt);
                break;    // there are no duplicates to remove.
            }
            
            else
                dataSetIt++;
        }
    }
    
    return found;
}

DataSet SystemHandler::getDataSet(const string& dssName, int sequenceNumber)
{
    for (const auto& dataSet : dataSetCollection[dssName])
    {
        if (dataSet.getSequenceNumber() == sequenceNumber)
            return dataSet;
    }
    
    throw logic_error("Requested data set not found");  // No match found
}


void SystemHandler::search(const string &dssName, const map<string, string> &queryFields, vector<SetDescriptor> &descriptorList)
{
    if (queryFields.empty())    // return all the SetDescriptor fields, no search at all.
    {
        for (const auto& dataSet : dataSetCollection[dssName])
        {
            descriptorList.push_back(dataSet.getSetDescriptor());
        }
    }

    else    // search required...
    {
        /* For each DataSet check if it has the specified query values. Only the SetDescriptors with all their fields matching are copied. */

        for (const auto& dataSet : dataSetCollection[dssName])
        {
            string key, value;
            bool foundAll = true;
            auto setDescriptor = dataSet.getSetDescriptor();
            
            for (const auto& queryIt : queryFields)
            {
                key = queryIt.first;
                value = queryIt.second;
                
                if (Utils::isIntegerString(value))
                {
                    if (setDescriptor.hasValue(key, atoi(value.c_str())))
                        foundAll = foundAll && true; // true only if it was true at the previous iteration, i.e. it has also the previous value
                    else
                        foundAll = false;
                }
                
                else if (Utils::isFloatString(value))
                {
                    if (setDescriptor.hasValue(key, (float) atof(value.c_str())))
                        foundAll = foundAll && true;
                    else
                        foundAll = false;
                }
                
                else if (setDescriptor.hasValue(key, value))
                    foundAll = foundAll && true;
                
                else
                    foundAll = false;
            }
            
            if (foundAll)
                descriptorList.push_back(setDescriptor);
        } 
    }
}



/* Private functions */

bool SystemHandler::parse(const string &conf)
{
    string dssName = Utils::removeSubstring(conf, ".conf");
    DataSetSpecifier dss {dssName};                                  // Create an empty DSS
    
    ifstream stream {(confDirectory + "/" + conf).c_str(), ios::in };
    
    if (!stream.is_open())
        return false;
    
    string buffer;                              // buffer for the read lines
    getline(stream, buffer);
    
    if (buffer.substr(0, Protocol::Syntax::SD_MARKER.size()) != Protocol::Syntax::SD_MARKER)              // Whether the first line is "SD" (following spaces are ignored)
        throw ParseException(conf + ": invalid Set Descriptor Line Marker (line 1)"); 
    
    string fieldName, fieldType;
    getline(stream, buffer);                    // discard the blank line after "SD"
    
    while (getline(stream, buffer) && !Utils::isBlankLine(buffer))              // parse untile the blank line before "DI"
    {
        istringstream parser {buffer};
        parser >> fieldName >> fieldType;
        addSetDescriptorField(fieldName, fieldType, dss);
        fieldName = fieldType = "";                             // empty the temp strings.
    }
    
    getline(stream, buffer);                                    // line with the "DI" marker
    
    if (buffer.substr(0, Protocol::Syntax::DI_MARKER.size()) != Protocol::Syntax::DI_MARKER)
        throw ParseException(conf + ": Invalid Data Items Marker");
    
    string fileName, fileType, fileTag, fileGroup; 
    getline(stream, buffer);
    
    while(getline(stream, buffer) && !Utils::isBlankLine(buffer)) 
    {
        istringstream parser {buffer};
        parser >> fileName >> fileType >> fileTag >> fileGroup;
        addDataItem(fileName, fileTag, fileGroup, dss);
        fileName = fileType = fileTag = fileGroup = "";
    }

    dssArray.insert({dssName, dss});
    return true;
}


void SystemHandler::addSetDescriptorField(const string &name, const string &type, DataSetSpecifier &dss)
{
    if (!type.empty())
    {
        if (type == STRING_TYPE)
        {
            if (!dss.getSetDescriptor().addKey<string>(name))
            {
                throw ParseException(string("Empty Set Descriptor String Field Name"));
            }
        }
        
        else if (type == INTEGER_TYPE)
        {
            if (!dss.getSetDescriptor().addKey<int>(name))
            {
                throw ParseException(string("Empty Set Descriptor Int Field Name"));
            }
        }

        else if (type == REAL_TYPE)
        {
            if (!dss.getSetDescriptor().addKey<float>(name))
            {
                throw ParseException(string("Empty Set Descriptor Float Field Name"));
            }
        }
            
        else throw ParseException("Invalid Set Descriptor Field Type");
    }
    
    else throw ParseException(string("Empty Set Descriptor Type Field"));
}


void SystemHandler::addDataItem(const string &name, const string &tag, const string &group, DataSetSpecifier &dss)
{
    if (name.empty() || tag.empty() || group.empty())
    {
        throw ParseException("Data Item Missing Field(s): name=" + name + ", tag=" + tag + ", group=" + group);
    }
    
    if (tag == NECESSARY_TAG)
    {
        if (group == INPUT_GROUP)
            dss.insertDataItem(DataItem(name, Tag::Necessary, Group::Input));
        
        else if (group == OUTPUT_GROUP)
            dss.insertDataItem(DataItem(name, Tag::Necessary, Group::Output));

        else throw ParseException("Invalid Data Item Group");

    }
    
    else if (tag == UNNECESSARY_TAG)
    {
        if (group == INPUT_GROUP)
            dss.insertDataItem(DataItem(name, Tag::Unnecessary, Group::Input));
        
        else if (group == OUTPUT_GROUP)
            dss.insertDataItem(DataItem(name, Tag::Unnecessary, Group::Output));
        
        else throw ParseException("Invalid Data Item Group");
    }
    
    else throw ParseException("Invalid Data Item Tag");
}

void SystemHandler::createIndex(const string &path)
{   
    ofstream stream {path.c_str(), ios::out | ios::app};     // do not overwrite existing files
    stream.close();
}

void SystemHandler::updateIndex(const string& path, const DataSet& dataSet)
{   
    ofstream stream {path.c_str(), ios::out | ios::app};     // do not overwrite existing files
    
    int sequenceNumber = dataSet.getSetDescriptor().getSequenceNumber();
    
    stream << sequenceNumber << " " << DATASET_DIR << sequenceNumber << " " << dataSet.getTimestamp() << endl;
    
    stream.close();
}


void SystemHandler::initFromIndex(const string &dssName)
{
    /* A SD-index has the following syntax:
     * 
     * ...
     * n timestamp DataSetn
     * ...
     * 
     * where n is a sequence number
     */
    
    string path = dataDirectory + "/" + dssName + "/" + SD_INDEX;
        
    istringstream parser;
    ifstream stream {path.c_str(), ios::in};
    string buffer, lastLine;
    int sequenceNumber = 0;
    
    if (stream.is_open() && stream.peek() != ifstream::traits_type::eof())   // check if the file is empty
    {
        while (getline(stream, buffer))
        {
            lastLine = buffer;  // save previous line
        }
        
        parser.str(lastLine);
        parser >> sequenceNumber;
        
        if (sequenceNumber <= 0)
            throw ParseException("Invalid integer used as last sequence number in SD-Index " + path);
        
        dssSequenceNumbers[dssName] = sequenceNumber;
    }
    
    stream.close();
}

void SystemHandler::fillDatSetCollection()
{
    try
    {
        for (const auto& dssIt : dssArray)
        {
            const string dssName = dssIt.first;
            vector<string> dataSetEntries;
            Filesystem::listDirectory(dataDirectory + "/" + dssName, dataSetEntries);

            for (const auto& dataSetIt : dataSetEntries)
            {
                if (dataSetIt != SD_INDEX)   // the not-"SD-Index" entries are the DataSet directories
                {
                    DataSet dataSet(dssIt.second);

                    string descrPath = dataDirectory + "/" + dssName + "/" + dataSetIt + "/" + DESCR_FILE;
                    ifstream input {descrPath.c_str(), ios::in};

                    string lineBuffer;
                    getline(input, lineBuffer);     // discard the "SD" line 
                    getline(input, lineBuffer);     // discard the blank line

                    while (getline(input, lineBuffer) && !Utils::isBlankLine(lineBuffer))   // read the lines <key value> until the blank line before the "DI" marker
                    {
                        string key, value;
                        istringstream parser {lineBuffer};
                        parser >> key >> value;

                        if (key == Protocol::Syntax::SN_SPECIAL_KEY)     // special case, the SN is also a class attribute.
                            dataSet.setSequenceNumber(atoi(value.c_str()));

                        else if (value.find("'") != string::npos)       // begin of a single quoted sentence
                        {
                            string temp = value;
                            parser >> value;
                            while (value.find("'") == string::npos)  // parse until the end of the sentence
                            {
                                temp += " " + value;  // build the sentence
                                parser >> value;
                            }
                            temp += " " + value;
                            value = temp;
                            dataSet.getSetDescriptor().addValue(key, value);
                        }

                        else if (Utils::isIntegerString(value))
                            dataSet.getSetDescriptor().addValue(key, atoi(value.c_str()));
                        
                        else if (Utils::isFloatString(value))
                            dataSet.getSetDescriptor().addValue(key, (float) atof(value.c_str()));
                        
                        
                        else    // simple string (a word)
                            dataSet.getSetDescriptor().addValue(key, value);
                    }

                    getline(input, lineBuffer);     // discard the "DI" line
                    getline(input, lineBuffer);     // discard the blank line

                    string itemName, itemFile;
                    size_t originalSize, encodedSize;
                    auto dataItems = dataSet.getDataItems();

                    while(!input.eof())
                    {
                        getline(input, lineBuffer);
                        istringstream parser {lineBuffer};
                        
                        parser >> itemName >> itemFile >> originalSize >> encodedSize;
                        
                        auto itemIt = find(dataItems.begin(), dataItems.end(), DataItem(itemName));
                        itemIt->setFileName(itemFile);
                        itemIt->isPresent(true);
                        itemIt->setOriginalSize(originalSize);
                        itemIt->setEncodedSize(encodedSize);
                    }

                    dataSetCollection[dssName].push_back(dataSet);
                }
            }
        }
        
    }
    catch (const runtime_error &e)
    {
        err << "Error during database initialization: " << e.what() << endl;
    }
}

