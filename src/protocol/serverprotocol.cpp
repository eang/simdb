/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <algorithm>
#include <sstream>

#include "serverprotocol.h"
#include "utils/utils.h"

using std::istringstream;
using std::map;
using std::ostream;
using std::ostringstream;
using std::string;
using std::vector;

using Entity::DataItem;
using Entity::Group;
using Entity::SetDescriptor;
using Entity::Tag;

using Protocol::Command;
using Protocol::DataSetRequest;
using Protocol::InsertRequest;
using Protocol::Handler;
using Protocol::RequestMessage;
using Protocol::SearchRequest;
using Protocol::Syntax::Error;

RequestMessage::RequestMessage(Command cmd) :
    command {cmd}
{}


Command RequestMessage::getCommand()
{
    return command;
}

void RequestMessage::setDssName(const string& name)
{
    dssName = name;
}

string RequestMessage::getDssName() const
{
    return dssName;
}


InsertRequest::InsertRequest() :
    RequestMessage {Command::Insert},
    sdSize {0}
{}

void InsertRequest::setSdSize(int s)
{
    sdSize = s;
}

const map<string, string>& InsertRequest::getSdFields() const
{
    return setDescriptorFields;
}

const vector<DataItem>& InsertRequest::getDataItems() const
{
    return dataItems;
}

void InsertRequest::addSetDescriptorField(const string& key, const string& value)
{
    setDescriptorFields.insert({key, value});
}
        
void InsertRequest::addDataItem(const DataItem& dataItem)
{
    dataItems.push_back(dataItem);
}

void InsertRequest::addItemOriginalSize(const std::string& itemName, size_t s)
{
    auto itemIt = find(dataItems.begin(), dataItems.end(), DataItem(itemName));

    if (itemIt == dataItems.end())
        throw Error(itemName + " is not a valid Data Item name");

    itemIt->setOriginalSize(s);
}

DataSetRequest::DataSetRequest(Command cmd) :
    RequestMessage {cmd},
    sequenceNumber {0}
{}

void DataSetRequest::setSequenceNumber(int sn)
{
    sequenceNumber = sn;
}

int DataSetRequest::getSequenceNumber() const
{
    return sequenceNumber;
}


SearchRequest::SearchRequest() :
    RequestMessage {Command::Search},
    sdQuerySize {0}
{}

void SearchRequest::setSdQuerySize(int s)
{
    sdQuerySize = s;
}

void SearchRequest::addSdQueryField(const string& key, const string& value)
{
    sdQueryFields.insert({key, value});
}

const map<string, string>& SearchRequest::getQueryFields() const
{
    return sdQueryFields;
}


RequestMessage *Handler::parseRequest(const string& request)
{
    istringstream parser {request};

    string command;
    parser >> command;

    if (command == Protocol::Syntax::INSERT)
        return parseInsertRequest(parser);

    if (command == Protocol::Syntax::REMOVE)
        return parseDataSetRequest(parser, Command::Remove);

    if (command == Protocol::Syntax::GET)
        return parseDataSetRequest(parser, Command::Get);

    if (command == Protocol::Syntax::SEARCH)
        return parseSearchRequest(parser);

    if (command == Protocol::Syntax::SPECLIST)
        return new RequestMessage(Command::Speclist);

    throw Error("Invalid request command (" + command +")");
}


string Handler::createGetResponse(int sequenceNumber, const map<string, string>& sdFields, const map<string, string>& dataItems)
{
    ostringstream risp;
    
    /* The response starts with "0 SD n", where n is the SetDescriptor fields number, including the "extra" field for the sequence number */
    risp << Protocol::Syntax::OK << '\n';
    risp << Protocol::Syntax::SD_MARKER << ' ' << sdFields.size() + 1 << '\n';
    risp << Protocol::Syntax::SN_SPECIAL_KEY << ' ' << sequenceNumber << '\n';
    
    for (const auto& it : sdFields)
    {
        string value = it.second;
        
        if (!Utils::isIntegerString(value) && !Utils::isFloatString(value) && !Utils::isQuotedSentence(value))      // simple string, it must be single quoted 
            value = "'" + it.second + "'";
        
        risp << it.first << ' ' << value << '\n';
    }
    
    risp << Protocol::Syntax::DI_MARKER << ' ' << dataItems.size() << '\n';
    
    for (const auto& it : dataItems)
    {
        risp << it.first << ' ' << it.second << '\n';
    }
    
    risp << Protocol::Syntax::DIFILES_MARKER << ' ' << dataItems.size() << '\n';

    return risp.str();
}

string Handler::createSearchResponse(const vector<SetDescriptor>& descriptorList)
{
    ostringstream risp;
    
    risp << Protocol::Syntax::OK << '\n';
    risp << Protocol::Syntax::FOUND_MARKER << ' ' << descriptorList.size() << '\n';    // The response starts with "0 FOUND k"

    for (vector<SetDescriptor>::const_iterator descrIt = descriptorList.begin(); descrIt != descriptorList.end(); descrIt++)
    for (const auto& descr : descriptorList)
    {
        risp << Protocol::Syntax::SD_MARKER << ' ' << descr.size() << '\n';
        risp << Protocol::Syntax::SN_SPECIAL_KEY << ' ' << descr.getSequenceNumber() << '\n';  // "... SD n SeqNum m"

        for (const auto& it : descr.getStringFields())
        {
            string value = it.second;
            
            if (!Utils::isQuotedSentence(value))    // sentences are already single quoted: if it's not a sentence then it's a word and we must single quote it.
                value = "'" + it.second + "'";
            
            risp << it.first << ' ' << value << '\n';
        }

        for (const auto& it : descr.getIntFields())
        {
            risp << it.first << ' ' << it.second << '\n';
        }

        for (const auto& it : descr.getFloatFields())
        {
            risp << it.first << ' ' << it.second << '\n';
        }
    }

    return risp.str();
}


InsertRequest *Handler::parseInsertRequest(istringstream& parser)
{
    auto insertRequest = new InsertRequest();

    string trash;
    parser >> trash;    // discard the "DSS" marker
    
    string dssName;
    parser >> dssName;
    insertRequest->setDssName(dssName);

    parser >> trash;    // discard the "SD" marker
        
    int sdSize;
    parser >> sdSize;
    insertRequest->setSdSize(sdSize);

    string key, value, temp;
    for (int i = 0; i < sdSize; i++)
    {
        parser >> key;
        parser >> value;
        
        if (!Utils::isQuoted(value)  && value.find("'") != string::npos)   // value is the begin of a multi-word string (e.g. 'value asd asd'), i.e. the begin of a sentence
        {
            value = Utils::parseUntilQuote(parser, value);
        }
        
        insertRequest->addSetDescriptorField(key, value);
    }

    parser >> trash;   // discard the "DI" marker
    
    int itemsNumber;
    parser >> itemsNumber;
    
    string itemName, fileName;
    for (int i = 0; i < itemsNumber; i++)
    {
        parser >> itemName >> fileName;
        insertRequest->addDataItem(DataItem(itemName, Tag::Unspecified, Group::Unspecified, fileName));
    }

    parser >> trash;   // discard the "DIFILES" marker

    int filesNumber;
    parser >> filesNumber;
        
    size_t fileSize;
    for (int i = 0; i < filesNumber; i++)
    {
        parser >> itemName >> fileSize;
        insertRequest->addItemOriginalSize(itemName, fileSize);
    }

    return insertRequest;
}


DataSetRequest *Handler::parseDataSetRequest(istringstream& parser, Command cmd)
{
    auto request = new DataSetRequest(cmd);

    int sn;
    parser >> sn;
    request->setSequenceNumber(sn);

    string dssName, trash;
    parser >> trash;        // discard the "DSS" marker
    parser >> dssName;
    request->setDssName(dssName);

    return request;
}


SearchRequest *Handler::parseSearchRequest(istringstream& parser)
{
    auto searchRequest = new SearchRequest();

    string trash;
    parser >> trash;  // discard the "DSS" marker
    
    string dssName;
    parser >> dssName;
    searchRequest->setDssName(dssName);
    
    parser >> trash;    // discard the "SD" marker
        
    int sdQuerySize = 0;
    
    parser >> sdQuerySize;
    searchRequest->setSdQuerySize(sdQuerySize);
    
    string key, value;
    for (int i = 0; i < sdQuerySize; i++)
    {
        parser >> key >> value;
        
        if (!Utils::isQuoted(value) && value.find("'") != string::npos)
            value = Utils::parseUntilQuote(parser, value);
        
        searchRequest->addSdQueryField(key, value);
    }

    return searchRequest;
}

string Protocol::commandToString(Command cmd)
{
    switch (cmd)
    {
        case Command::Insert:
            return Syntax::INSERT;
        case Command::Remove:
            return Syntax::REMOVE;
        case Command::Get:
            return Syntax::GET;
        case Command::Search:
            return Syntax::SEARCH;
        case Command::Speclist:
            return Syntax::SPECLIST;
        case Command::Unspecified:
        default:
            return "UNKNOWN";
    }
}

namespace Protocol
{
    ostream& operator<<(ostream& os, const RequestMessage& msg)
    {
        os << "['" << commandToString(msg.command) << "']";
        return os;
    }

    ostream& operator<<(ostream& os, const InsertRequest& msg)
    {
        string fields;
        for (const auto& it : msg.setDescriptorFields)
        {
            fields += "<" + it.first + "=" + it.second + "> ";
        }

        string files;
        for (const auto& it : msg.dataItems)
        {
            files += "<" + it.getName() + ", " + it.getFileName() + "> ";
        }

        string sizes;
        for (const auto& it : msg.dataItems)
        {
            sizes += "<" + it.getName() + ", " + Utils::toString(it.getOriginalSize()) + "> ";
        }

        os << "['" << commandToString(msg.command) << "' '" << msg.dssName << "' '" << Syntax::SD_MARKER << "' '"
            << msg.sdSize << "' '" << fields << "' '" << Syntax::DI_MARKER << "' '" << msg.dataItems.size() << "' '" << files << "' '"
            << Syntax::DIFILES_MARKER << "' '" << msg.dataItems.size() << "' '" << sizes << "']";
            
        return os;
    }

    ostream& operator<<(ostream& os, const DataSetRequest& msg)
    {
        os << "['" << commandToString(msg.command) <<  "' '" << msg.sequenceNumber << "' '" << msg.dssName << "']";

        return os;
    }

    ostream& operator<<(ostream& os, const SearchRequest& msg)
    {
        string fields;
        for (const auto& it : msg.sdQueryFields)
        {
            fields += "<" + it.first + "=" + it.second + ">";
        }

        os << "['" << commandToString(msg.command) << "' '" << msg.dssName << "' '" << Syntax::SD_MARKER << "' '"
            << msg.sdQuerySize << "' '" << fields << "']";

        return os;
    }
}
