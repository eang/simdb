/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <cstdlib>
#include <sstream>

#include "clientprotocol.h"
#include "filesystem/filesystem.h"
#include "utils/utils.h"

using std::endl;
using std::map;
using std::istringstream;
using std::ostream;
using std::ostringstream;
using std::string;
using std::vector;

using Protocol::GetResponse;
using Protocol::InsertResponse;
using Protocol::Handler;
using Protocol::ResponseCode;
using Protocol::ResponseMessage;
using Protocol::SearchResponse;
using Protocol::SpeclistResponse;
using Protocol::Syntax::Error;

/* Define ResponseMessage class */

ResponseMessage::ResponseMessage(ResponseCode code) :
    responseCode {code}
{}

ResponseCode ResponseMessage::getResponseCode() const
{
    return responseCode;
}

/* Define InsertResponse class */

InsertResponse::InsertResponse(ResponseCode code) :
    ResponseMessage {code}
{}

void InsertResponse::setSequenceNumber(int sn)
{
    if (sn <= 0)
        throw Error("DataSet sequence number must be a positive integer");

    sequenceNumber = sn;
}

/* Define GetResponse class */

GetResponse::GetResponse(ResponseCode code) :
    ResponseMessage {code}
{}

void GetResponse::setSdSize(int s)
{
    sdSize = s;
}

int GetResponse::getSdSize() const
{
    return sdSize;
}

void GetResponse::setSequenceNumber(int sn)
{
    sequenceNumber = sn;
}

int GetResponse::getSequenceNumber() const
{
    return sequenceNumber;
}

void GetResponse::setFilesNumber(int n)
{
    filesNumber = n;
}

int GetResponse::getFilesNumber() const
{
    return filesNumber;
}

const map<string, string>& GetResponse::getSdFields() const
{
    return setDescriptorFields;
}

const map< string, string >& GetResponse::getDataItems() const
{
    return dataItems;
}

map< string, string >& GetResponse::getDataItems()
{
    return dataItems;
}

void GetResponse::addSetDescriptorField(const string& key, const string& value)
{
    setDescriptorFields.insert({key, value});
}

void GetResponse::addDataItem(const string& itemName, const string& itemFile)
{
    dataItems.insert({itemName, itemFile});
}

/* Define SearchResponse class */

SearchResponse::SearchResponse(ResponseCode code) :
    ResponseMessage {code}
{}

void SearchResponse::setFoundCount(int c)
{
    foundCount = c;
}

int SearchResponse::getFoundCount() const
{
    return foundCount;
}

void SearchResponse::addSdFields(int sn, const map<string, string>& fields)
{
    setDescriptorFields.insert({sn, fields});
}

/* Define SpeclistResponse class */

SpeclistResponse::SpeclistResponse(ResponseCode code) : ResponseMessage(code) {}

void SpeclistResponse::setDssNumber(int n)
{
    dssNumber = n;
}

void SpeclistResponse::addDssEntry(const string& dssEntry)
{
    dssList.push_back(dssEntry);
}


/* Functions creating the requests starting from "structured" input */

string Handler::createInsertRequest(const string& dssName, map<string, string>& sdFields, const map<string, string>& dataItems)
{
    /* Step 1, preprocessing the SetDescriptor values to single quote the string ones, if not already quoted */

    for (map<string, string>::iterator fieldsIt = sdFields.begin(); fieldsIt != sdFields.end(); fieldsIt++)
    for (auto& it : sdFields)
    {
        string value = it.second;

        if (value.find(" ") != string::npos)        // value is a sentence
        {
            if (!Utils::isQuotedSentence(value))
                it.second = "'" + value + "'";
        }

        else if (!Utils::isIntegerString(value) && !Utils::isFloatString(value) && !Utils::isQuoted(value)) // there is no space (i.e. not a sentence), neither an int nor a float, so it's an unquoted word
            it.second = "'" + value + "'";
    }

    /* Step 2, ready to build the request string message */

    ostringstream request;

    request << Protocol::Syntax::INSERT << '\n';
    request << Protocol::Syntax::DSS_MARKER << ' ' << dssName << '\n';
    request << Protocol::Syntax::SD_MARKER << ' ' << sdFields.size() << '\n';

    for (const auto& it : sdFields)
    {
        request << it.first << ' ' << it.second << '\n';
    }

    request << Protocol::Syntax::DI_MARKER << ' ' << dataItems.size() << '\n';

    for (const auto& it : dataItems)
    {
        request << it.first << ' ' << it.second << '\n';
    }

    request << Protocol::Syntax::DIFILES_MARKER << ' ' << dataItems.size() << '\n';

    for (const auto& it : dataItems)
    {
        request << it.first << ' ' << Filesystem::getFileSize(it.second) << '\n';
    }

    return request.str();
}

string Handler::createRemoveRequest(const string& sequenceNumber, const string& dssName)
{
    string request = Protocol::Syntax::REMOVE + ' ' + sequenceNumber + '\n';
    request += Protocol::Syntax::DSS_MARKER + ' ' + dssName;

    return request;
}

string Handler::createGetRequest(const string& sequenceNumber, const string& dssName)
{
    string request = Protocol::Syntax::GET + ' ' + sequenceNumber + '\n';
    request += Protocol::Syntax::DSS_MARKER + ' ' + dssName;

    return request;
}

string Handler::createSearchRequest(const string& dssName, map<string, string>& sdQueryFields)
{
    /* Step 1, preprocessing the SetDescriptor values to single quote the string ones, if not already quoted */

    for (auto& it : sdQueryFields)
    {
        string value = it.second;

        if (value.find(" ") != string::npos)        // value is a sentence
        {
            if (!Utils::isQuotedSentence(value))
                it.second = "'" + value + "'";
        }

        else if (!Utils::isIntegerString(value) && !Utils::isFloatString(value) && !Utils::isQuoted(value)) // there is no space (i.e. not a sentence), neither an int nor a float, so it's an unquoted word
            it.second = "'" + value + "'";
    }

    /* Step 2, ready to build the request string message */

    ostringstream request;

    request << Protocol::Syntax::SEARCH << '\n';
    request << Protocol::Syntax::DSS_MARKER << ' ' << dssName << '\n';
    request << Protocol::Syntax::SD_MARKER << ' ' << sdQueryFields.size() << '\n';

    for (const auto& it : sdQueryFields)
    {
        request << it.first << ' ' << it.second << '\n';
    }

    return request.str();
}

/* Functions creating the requests starting from a "raw" input, e.g. a single string */

string Handler::createInsertRequest(const string& input, map<string, string>& dataItems)
{
    string dssName, temp1, temp2, trash;
    map<string, string> sdFields;

    if (input.find(Protocol::Syntax::DI_MARKER) == string::npos)
        throw Error("Missing " + Protocol::Syntax::DI_MARKER + " marker");

    istringstream parser {input};

    parser >> trash;    // discard the initial command
    parser >> dssName;

    parser >> temp1;

    while (temp1 != Protocol::Syntax::DI_MARKER && !parser.eof())
    {
        parser >> temp2;

        if (temp2.find("'") != string::npos && !Utils::isQuoted(temp2))     // begin of a single quoted sentence
        {
            temp2 = Utils::parseUntilQuote(parser, temp2);
        }

        sdFields.insert({temp1, temp2});

        parser >> temp1;
    }

    while (!parser.eof())
    {
        parser >> temp1 >> temp2;
        dataItems.insert({temp1, temp2});
    }

    return createInsertRequest(dssName, sdFields, dataItems);
}

string Handler::createRemoveRequest(const string& input)
{
    string sequenceNumber, dssName, trash;

    istringstream parser {input};

    parser >> trash;
    parser >> sequenceNumber >> dssName;

    if (sequenceNumber.empty() || dssName.empty())
        throw Error("Incomplete REMOVE command");

    return Protocol::Syntax::REMOVE + ' ' + sequenceNumber + '\n' + Protocol::Syntax::DSS_MARKER + ' ' + dssName + '\n';
}


string Handler::createGetRequest(const string& input)
{
    string sequenceNumber, dssName, trash;

    istringstream parser {input};

    parser >> trash;
    parser >> sequenceNumber >> dssName;

    if (sequenceNumber.empty() || dssName.empty())
        throw Error("Incomplete GET command");

    return Protocol::Syntax::GET + ' ' + sequenceNumber + '\n' + Protocol::Syntax::DSS_MARKER + ' ' + dssName + '\n';
}


string Handler::createSearchRequest(const string& input)
{
    istringstream parser {input};

    string dssName, trash;
    parser >> trash;   // discard the "SEARCH" command
    parser >> dssName;

    if (dssName.empty())
        throw Error("Missing Data Set Specifier name");

    string fieldName, fieldValue;
    map<string, string> sdQueryFields;

    while (!parser.eof())
    {
        parser >> fieldName >> fieldValue;

        if (!Utils::isQuoted(fieldValue) && fieldValue.find("'") != string::npos)   // begin of a single quoted sentence
        {
            fieldValue = Utils::parseUntilQuote(parser, fieldValue);
        }

        sdQueryFields.insert({fieldName, fieldValue});
    }

    return createSearchRequest(dssName, sdQueryFields);
}

ResponseMessage Handler::parseResponseMessage(const string& response)
{
    if (response == Protocol::Syntax::OK)
        return {ResponseCode::Ok};

    if (response == Protocol::Syntax::WRONG_AUTH)
        return {ResponseCode::WrongAuth};

    if (response == Protocol::Syntax::INCOMPLETE_SET)
        return {ResponseCode::IncompleteSet};

    if (response == Protocol::Syntax::NO_SUCH_SPEC)
        return {ResponseCode::NoSuchSpec};

    if (response == Protocol::Syntax::TOO_MUCH_DATA)
        return {ResponseCode::TooMuchData};

    if (response == Protocol::Syntax::NO_SUCH_SET)
        return {ResponseCode::NoSuchSet};

    if (response == Protocol::Syntax::WRONG_TYPE)
        return {ResponseCode::WrongType};

    if (response == Protocol::Syntax::UNKNOWN_NAME)
        return {ResponseCode::UnknownName};

    if (response == Protocol::Syntax::UNKNOWN_FIELD)
        return {ResponseCode::UnknownField};

    if (response == Protocol::Syntax::GENERIC_ERROR)
        return {ResponseCode::GenericError};

    throw Error(response + " is an invalid response code");
}

InsertResponse Handler::parseInsertResponse(const string& response)
{
    istringstream parser {response};

    string code;
    parser >> code;

    if (code == Protocol::Syntax::OK)
    {
        InsertResponse insertResponse {ResponseCode::Ok};

        int sn;
        parser >> sn;
        insertResponse.setSequenceNumber(sn);

        return insertResponse;
    }

    if (code == Protocol::Syntax::GENERIC_ERROR)
    {
        return {ResponseCode::GenericError};
    }

    throw Error(code + " is an invalid response code for an INSERT response message");
}

GetResponse Handler::parseGetResponse(const string& response)
{
    istringstream parser {response};

    string code;
    parser >> code;

    if (code == Protocol::Syntax::OK)
    {
        GetResponse getResponse {ResponseCode::Ok};

        string trash, key, value;
        parser >> trash;   // discard the SD marker

        int sdSize, itemNumber;
        parser >> sdSize;

        int sequenceNumber;
        map<string, string> fields;
        for (int i = 0; i < sdSize; i++)
        {
            parser >> key >> value;

            if (key == Protocol::Syntax::SN_SPECIAL_KEY)      // the sequence number needs a particular treatment
            {
                sequenceNumber = atoi(value.c_str());
                continue;   // don't add the pait <SN_SPECIAL_KEY, sn> to the map
            }

            else if (!Utils::isQuoted(value)  && value.find("'") != string::npos)   // value is the begin of a sentence
            {
                value = Utils::parseUntilQuote(parser, value);
            }

            else if (Utils::isQuoted(value))
                value = value.substr(1, value.length() - 2);    // unquote the simple strings

            getResponse.addSetDescriptorField(key, value);
        }

        getResponse.setSequenceNumber(sequenceNumber);

        parser >> trash;        // discard the DI marker
        parser >> itemNumber;

        string itemName, itemFile;
        for (int i = 0; i < itemNumber; i++)
        {
            parser >> itemName >> itemFile;
            getResponse.addDataItem(itemName, itemFile);
        }

        int filesNumber;
        parser >> trash;    // discard the DIFILES marker
        parser >> filesNumber;

        getResponse.setFilesNumber(filesNumber);

        return getResponse;
    }

    if (code == Protocol::Syntax::NO_SUCH_SPEC)
    {
        return {ResponseCode::NoSuchSpec};
    }

    if (code == Protocol::Syntax::NO_SUCH_SET)
    {
        return {ResponseCode::NoSuchSet};

    }

    if (code == Protocol::Syntax::GENERIC_ERROR)
    {
        return {ResponseCode::GenericError};
    }

    throw Error(code + " is an invalid response code for a GET response message");
}

SearchResponse Handler::parseSearchResponse(const string& response)
{
    istringstream parser {response};

    string code;
    parser >> code;

    if (code == Protocol::Syntax::OK)
    {
        SearchResponse searchResponse {ResponseCode::Ok};

        string trash, key, value, temp;
        parser >> trash;   // discard the "FOUND" marker

        int foundCount, sdSize;
        parser >> foundCount;
        searchResponse.setFoundCount(foundCount);

        for (int i = 0; i < foundCount; i++)
        {
            parser >> trash; // discard the SD marker
            parser >> sdSize;

            int sequenceNumber;
            map<string, string> fields;
            for (int j = 0; j < sdSize; j++)
            {
                parser >> key >> value;

                if (key == Protocol::Syntax::SN_SPECIAL_KEY)      // the sequence number needs a particular treatment
                {
                    sequenceNumber = atoi(value.c_str());
                    continue;   // don't add the pair <SN_SPECIAL_KEY, sn> to the map
                }

                else if (!Utils::isQuoted(value)  && value.find("'") != string::npos)   // value is the begin of a sentence
                {
                    value = Utils::parseUntilQuote(parser, value);
                }

                else if (Utils::isQuoted(value))
                    value = value.substr(1, value.length() - 2);    // unquote the simple strings

                fields.insert({key, value});
            }

            searchResponse.addSdFields(sequenceNumber, fields);
        }

        return searchResponse;
    }

    if (code == Protocol::Syntax::NO_SUCH_SPEC)
    {
        return {ResponseCode::NoSuchSet};
    }

    if (code == Protocol::Syntax::UNKNOWN_FIELD)
    {
        return {ResponseCode::UnknownField};
    }

    if (code == Protocol::Syntax::GENERIC_ERROR)
    {
        return {ResponseCode::GenericError};
    }

    throw Error(code + " is an invalid response code for an SEARCH response message");
}


SpeclistResponse Handler::parseSpeclistResponse(const string& response)
{
    istringstream parser {response};

    string code;
    parser >> code;

    if (code == Protocol::Syntax::OK)
    {
        SpeclistResponse speclistResponse {ResponseCode::Ok};

        string trash;
        parser >> trash;   // discard the "FOUND" marker

        int n;
        parser >> n;
        speclistResponse.setDssNumber(n);

        string dssEntry;
        for (int i = 0; i < n; i++)
        {
            parser >> dssEntry;
            speclistResponse.addDssEntry(dssEntry);
        }

        return speclistResponse;
    }

    if (code == Protocol::Syntax::GENERIC_ERROR)
    {
        SpeclistResponse speclistResponse {ResponseCode::GenericError};
        speclistResponse.setDssNumber(0);

        return speclistResponse;
    }

    throw Error("Invalid response code for a SPECLIST response message");

}

string Handler::getExtendedErrorMessage(ResponseCode code)
{
    switch (code)
    {
        case ResponseCode::WrongAuth:   // unused
            return "Failed to authenticate with server.";
        case ResponseCode::IncompleteSet:
            return "Missing *necessary* files, check the related Data Set Specifier";
        case ResponseCode::NoSuchSpec:
            return "Wrong DSS name, check the name provided within the SPECLIST command response.";
        case ResponseCode::TooMuchData:
            return "Too much Set Descriptor fields and/or Data Items in INSERT request";
        case ResponseCode::NoSuchSet:
            return "The sequence number provided is wrong, use the SEARCH command to see the valid sequence numbers.";
        case ResponseCode::WrongType:
            return "A Set Descriptor value has a wrong type, check server log for more info.";
        case ResponseCode::UnknownName:
            return {};
        case ResponseCode::UnknownField:
            return "A Set Descriptor value has a bad syntax, check protocol rules.";
        case ResponseCode::GenericError:
            return "Generic protocol error, check server log and/or server response";
        case ResponseCode::Ok:
        case ResponseCode::Unspecified:
        default:
            return {};
    }
}

string Protocol::responseCodeToString(ResponseCode code)
{
    switch (code)
    {
        case ResponseCode::Ok:
            return Message::OK;
        case ResponseCode::WrongAuth:
            return Message::WRONG_AUTH;
        case ResponseCode::IncompleteSet:
            return Message::INCOMPLETE_SET;
        case ResponseCode::NoSuchSpec:
            return Message::NO_SUCH_SPEC;
        case ResponseCode::TooMuchData:
            return Message::TOO_MUCH_DATA;
        case ResponseCode::NoSuchSet:
            return Message::NO_SUCH_SET;
        case ResponseCode::WrongType:
            return Message::WRONG_TYPE;
        case ResponseCode::UnknownName:
            return Message::UNKNOWN_NAME;
        case ResponseCode::UnknownField:
            return Message::UNKNOWN_FIELD;
        case ResponseCode::GenericError:
            return Message::GENERIC_ERROR;
        case ResponseCode::Unspecified:
        default:
            return "UNKNOWN";
    }
}

namespace Protocol
{
    ostream& operator<<(ostream& os, const ResponseMessage& msg)
    {
        os << "> Response code: [" << static_cast<int>(msg.responseCode) << ": '" << responseCodeToString(msg.responseCode) << "']" << endl;

        return os;
    }

    ostream& operator<<(std::ostream& os, const InsertResponse& msg)
    {
        if (msg.responseCode != ResponseCode::Ok)
            // TODO: why without cast there is a "ambiguous overload for operator <<" ?
            os << "> Response code: [" << static_cast<int>(msg.responseCode) << ": '" << responseCodeToString(msg.responseCode) << "']";

        else
            os << "> Response code: [" << static_cast<int>(msg.responseCode) << ": '" << responseCodeToString(msg.responseCode) << "']" << endl << "> Response content: " << endl << "Sequence number = " << msg.sequenceNumber << endl;

        return os;
    }

    ostream& operator<<(ostream& os, const GetResponse& msg)
    {
        if (msg.responseCode != ResponseCode::Ok)
            os << "> Response code: [" << static_cast<int>(msg.responseCode) << ": '" << responseCodeToString(msg.responseCode) << "']";

        else
        {
            os << "> Response code: [" << static_cast<int>(msg.responseCode) << ": '" << responseCodeToString(msg.responseCode) << "']" << endl << "> Response content: " << endl;


            os << "Set Descriptor: [SeqNum " << msg.sequenceNumber << "]\n";

            for (const auto& it : msg.setDescriptorFields)
            {
                os << "* " << it.first << " = " << it.second << "\n";
            }

            os << "\nData Items: " << endl;

            for (const auto& it : msg.dataItems)
            {
                os << "* " << it.first << ": " << it.second << endl;
            }
        }

        return os;
    }

    ostream& operator<<(ostream& os, const SearchResponse& msg)
    {
        if (msg.responseCode != ResponseCode::Ok)
            os << "> Response code: [" << static_cast<int>(msg.responseCode) << ": '" << responseCodeToString(msg.responseCode) << "']";

        else
        {
            os << "> Response code: [" << static_cast<int>(msg.responseCode) << ": '" << responseCodeToString(msg.responseCode) << "']" << endl << "> Response content: " << endl;

            if (msg.foundCount == 0)
                os << "Nothing found" << endl;

            else
            {
                os << Syntax::FOUND_MARKER << " " << msg.foundCount << " Data Set" << endl;

                for (const auto& mapIt : msg.setDescriptorFields)
                {
                    os << "[SN " << mapIt.first << "] { ";

                    const auto& fields = mapIt.second;

                    for (const auto& fieldsIt : fields)
                    {

                        os << fieldsIt.first << " = " << fieldsIt.second << "; ";
                    }

                    os << "}" << endl;
                }
            }
        }

        return os;
    }

    ostream& operator<<(ostream& os, const SpeclistResponse& msg)
    {
        string entries;
        int index = 1;

        for (const auto& it : msg.dssList)
        {
            entries += Utils::toString(index) + ". " + it + "\n";
            index++;
        }

        string dssMessage = msg.dssNumber > 0 ? " entries: " : " entries.";
        os << "> Response code: [ " << static_cast<int>(msg.responseCode) << " : '" << responseCodeToString(msg.responseCode) << "' ]" << endl << "> Response content: " << endl << "Found " << msg.dssNumber << dssMessage << endl << entries;

        return os;
    }
}
