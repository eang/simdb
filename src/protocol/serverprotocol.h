/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef SERVER_PROTOCOL_H
#define SERVER_PROTOCOL_H

#include <map>
#include <string>
#include <vector>

#include "entity/dataitem.h"
#include "entity/setdescriptor.h"
#include "protocol.h"

namespace Protocol
{

   /**
    * @brief A generic request message of the protocol
    *
    * A request message is defined by a specific command and by the DSS name provided by the client for the server operation.
    */
    class RequestMessage
    {
    public:

        RequestMessage(Command cmd);

        Command getCommand();
        void setDssName(const std::string& name);
        std::string getDssName() const;

        friend std::ostream& operator<<(std::ostream& os, const RequestMessage& msg);

    protected:

        Command command;
        std::string dssName;
    };



   /**
    * @brief An INSERT request message.
    *
    * A INSERT message is defined by the DSS name (like every RequestMessage), by the fields number in the SetDescriptor and by their definition, by the new DataItems of the DataSet and finally by the DataItems filenames and original sizes.
    */
    class InsertRequest : public RequestMessage
    {
    public:
        InsertRequest();

        void setSdSize(int s);
        const std::map<std::string, std::string>& getSdFields() const;
        const std::vector<Entity::DataItem>& getDataItems() const;
        void addSetDescriptorField(const std::string& key, const std::string& value);
        void addDataItem(const Entity::DataItem& dataItem);
        void addItemOriginalSize(const std::string& itemName, size_t s);

        friend std::ostream& operator<<(std::ostream& os, const InsertRequest& msg);

    private:

        int sdSize;                                             /** Numero di campi del SetDescriptor forniti nella richiesta */
        std::map<std::string, std::string> setDescriptorFields;     /** Associazioni chiave-valore del SetDescriptor */
        std::vector<Entity::DataItem> dataItems;                    /** Lista DataItem ricevuti */
    };


   /**
    * @brief A GET or REMOVE request message.
    *
    * A GET or REMOVE  message is defined by the DataSet sequence number to be retrieved or removed.
    */
    class DataSetRequest : public RequestMessage
    {
    public:
        DataSetRequest(Command cmd);

        void setSequenceNumber(int sn);
        int getSequenceNumber() const;

        friend std::ostream& operator<<(std::ostream& os, const DataSetRequest& msg);

    private:

        int sequenceNumber;
    };

   /**
    * @brief A SEARCH request message.
    *
    * A SEARCH request message is defined by the DSS name (like every RequestMessage) and by the number of query to the server.
    */
    class SearchRequest : public RequestMessage
    {

    public:

        SearchRequest();

        void setSdQuerySize(int s);
        void addSdQueryField(const std::string& key, const std::string& value);
        const std::map<std::string, std::string>& getQueryFields() const;

        friend std::ostream& operator<<(std::ostream& os, const SearchRequest& msg);

    private:

        int sdQuerySize;
        std::map<std::string, std::string> sdQueryFields;

    };


   /**
    * @brief Convenience class to simplify the protocol messages handling.
    * This class is useful to parse the "raw" message received by the client into much more handy RequestMessage objects.
    * It also features convenience function to create response messages to GET and SEARCH requests.
    */
    class Handler
    {

    public:

       /** Parse a "raw" client request into a RequestMessage object.
        * @param request String with the "raw" request message.
        * @return Pointer to the resulting RequestMessage object.
        */
        static RequestMessage *parseRequest(const std::string& request);

        /* Create response messages to GET and SEARCH request messaes. */
        static std::string createGetResponse(int sequenceNumber, const std::map<std::string, std::string>& sdFields, const std::map<std::string, std::string>& dataItems);
        static std::string createSearchResponse(const std::vector<Entity::SetDescriptor>& descriptorList);

    private:

        /* Convenience functions for the parseRequest() main function. */
        static InsertRequest *parseInsertRequest(std::istringstream& parser);
        static DataSetRequest *parseDataSetRequest(std::istringstream& parser, Command cmd);
        static SearchRequest *parseSearchRequest(std::istringstream& parser);
    };

   /** Translate a request command from enum to string.
    * @param cmd A certain request command enum value.
    * @return The relevant string for the given command.
    */
    std::string commandToString(Command cmd);
}

#endif
