/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef CLIENT_PROTOCOL_H
#define CLIENT_PROTOCOL_H

#include <string>
#include <vector>
#include <map>
#include <set>

#include "protocol.h"

namespace Protocol
{
   /**
    * @brief A generic response message of the protocol.
    * A response message is defined by a return code which specifies whether the request has been accepted or there has been an error.
    */
    class ResponseMessage
    {
    public:

        ResponseMessage(ResponseCode code);

        ResponseCode getResponseCode() const;

        friend std::ostream& operator<<(std::ostream& os, const ResponseMessage& msg);

    protected:

        ResponseCode responseCode;
    };


   /**
    * @brief A response message to INSERT requests.
    * A response message to INSERT requests is simply defined through the sequence number assigned by the server to the given DataSet.
    */
    class InsertResponse : public ResponseMessage
    {
    public:
        InsertResponse(ResponseCode code);

        void setSequenceNumber(int sn);

        friend std::ostream& operator<<(std::ostream& os, const InsertResponse& msg);

    private:

        int sequenceNumber;
    };

   /**
    * @brief A response message to GET requests.
    * A response message to GET requests is defined by the SetDescriptor and by the DataItems of the requested DataSet.
    */
    class GetResponse : public ResponseMessage
    {
    public:
        GetResponse(ResponseCode code);

        void setSdSize(int s);
        int getSdSize() const;
        void setSequenceNumber(int sn);
        int getSequenceNumber() const;
        void setFilesNumber(int n);
        int getFilesNumber() const;
        const std::map<std::string, std::string>& getSdFields() const;
        const std::map<std::string, std::string>& getDataItems() const;
        std::map<std::string, std::string>& getDataItems();
        void addSetDescriptorField(const std::string& key, const std::string& value);
        void addDataItem(const std::string& itemName, const std::string& itemFile);

        friend std::ostream& operator<<(std::ostream& os, const GetResponse& msg);

    private:

        int sdSize;                                             /** SetDescriptor fields number, including the "extra" field for the sequence number. */
        int sequenceNumber;                                         /** Sequence number of the SetDescriptor/DataSet. */
        int filesNumber;                                            /** Files number in the response DIFILES section; it should be equal to the dataItems map size. */
        std::map<std::string, std::string> setDescriptorFields;     /** Map with the pairs <key, value> of the SetDescriptor. */
        std::map<std::string, std::string> dataItems;               /** Map with the pairs <DataItem name, "instance" file name>. */
    };


   /**
    * @brief A response message to SEARCH request.
    * A response message to SEARCH requests is defined by the SetDescriptors list matching the given query parameters provided by the client.
    */
    class SearchResponse : public ResponseMessage
    {
    public:
        SearchResponse(ResponseCode code);

        void setFoundCount(int c);
        int getFoundCount() const;
        void addSdFields(int sn, const std::map<std::string, std::string>& fields);

        friend std::ostream& operator<<(std::ostream& os, const SearchResponse& msg);

    private:

        int foundCount;                                                         /** Number of SetDescriptors matching the query. */

        std::map<int, std::map<std::string, std::string> > setDescriptorFields;     /** Map of maps ordered by the sequence number and holding the key-value associations. */
    };

   /**
    * @brief A response message to SPECLIST requests.
    * A reponse message to SPECLIST request may be negative, if no DSS are found in the system (error code 99).
    * If some DSS is found, the response shows how may they are and their names.
    */
    class SpeclistResponse : public ResponseMessage
    {
    public:

        SpeclistResponse(ResponseCode code);

        void setDssNumber(int n);
        void addDssEntry(const std::string& dssEntry);

        friend std::ostream& operator<<(std::ostream& os, const SpeclistResponse& msg);

    private:

        int dssNumber;                      /** DSS number within the system. */
        std::vector<std::string> dssList;       /** List of DSS names within the system. */
    };


   /**
    * @brief Convenience class to simplify the protocol messages handling.
    * This class is useful to parse the "raw" message received by the server into much more handy ResponseMessage objects.
    * It also features convenience function to create INSERT and SEARCH requests.
    */
    class Handler
    {
    public:

       /*
        * Create the "raw" messages with the client requests, starting from a "structured" input (i.e. single strings and containters).
        * The given input is formatted in a single string according to the protocol syntax.
        */
        static std::string createInsertRequest(const std::string& dssName, std::map<std::string, std::string>& sdFields, const std::map<std::string, std::string>& dataItems);
        static std::string createRemoveRequest(const std::string& sequenceNumber, const std::string& dssName);
        static std::string createGetRequest(const std::string& sequenceNumber, const std::string& dssName);
        static std::string createSearchRequest(const std::string& dssName, std::map<std::string, std::string>& sdQueryFields);

       /*
        * Create the "raw" messages with the client requests, starting form a "raw, non-structured" input (i.e. one single string).
        * The given string is parsed and a new string is formatted according to the protocol syntax, for simple REMOVE and GET requests.
        * For INSERT and SEARCH requests the given string is parsed and the above functions are called.
        */
        static std::string createInsertRequest(const std::string& input, std::map<std::string, std::string>& dataItems);
        static std::string createRemoveRequest(const std::string& input);
        static std::string createGetRequest(const std::string& input);
        static std::string createSearchRequest(const std::string& input);

       /*
        * Convenience functions to parse a server "raw"response into a much more handy ResponseMessage.
        * @param response The raw server response message.
        * @return The resulting ResponseMessage object.
        */
        static ResponseMessage parseResponseMessage(const std::string& response);
        static InsertResponse parseInsertResponse(const std::string& response);
        static GetResponse parseGetResponse(const std::string& response);
        static SearchResponse parseSearchResponse(const std::string& response);
        static SpeclistResponse parseSpeclistResponse(const std::string& response);

       /** Translate a response code from enum to an extended error message.
        * @param code a ResponseCode of a certain server response.
        * @return An extended error message (if code is different from "0 OK"), otherwise the "OK" string.
        */
        static std::string getExtendedErrorMessage(ResponseCode code);
    };


   /**
    * Translate a response code from enum to string.
    * @param code a ResponseCode of a certain server response.
    * @return the relevant string for the given response code.
    */
    std::string responseCodeToString(ResponseCode code);
}

#endif
