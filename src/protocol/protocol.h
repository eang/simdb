/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdexcept>
#include <string>

namespace Protocol
{
    enum class Command
    {
        Unspecified,
        Insert,
        Remove,
        Get,
        Search,
        Speclist

    };

    enum class ResponseCode : int
    {
        Ok,
        WrongAuth,
        IncompleteSet,
        NoSuchSpec,
        TooMuchData,
        NoSuchSet,
        WrongType,
        UnknownName,
        UnknownField,
        GenericError=99,
        Unspecified

    };

    namespace Syntax
    {
        const std::string FILE_ACK = "ACK";
        const std::string DSS_MARKER = "DSS";
        const std::string SD_MARKER = "SD";
        const std::string DI_MARKER = "DI";
        const std::string DIFILES_MARKER = "DIFILES";
        const std::string FOUND_MARKER = "FOUND";
        const std::string SN_SPECIAL_KEY = "SequenceNumber"; /** Extra key used within the "Descr" files, since the sequence number is a class attribute and not just a SetDescriptor field. */

        /* Commands strings */
        const std::string INSERT = "INSERT";
        const std::string REMOVE = "REMOVE";
        const std::string GET = "GET";
        const std::string SEARCH = "SEARCH";
        const std::string SPECLIST = "SPECLIST";

        /* Return codes strings */
        const std::string OK =  "0";
        const std::string WRONG_AUTH = "1";
        const std::string INCOMPLETE_SET = "2";
        const std::string NO_SUCH_SPEC = "3";
        const std::string TOO_MUCH_DATA = "4";
        const std::string NO_SUCH_SET = "5";
        const std::string WRONG_TYPE = "6";
        const std::string UNKNOWN_NAME = "7";
        const std::string UNKNOWN_FIELD = "8";
        const std::string GENERIC_ERROR = "99";

        /** Exception thrown when there is a syntax error during a request/response parsing. */
        class Error : public std::runtime_error
        {
            public:
                Error(const std::string& msg) : std::runtime_error(msg) {}
        };
    }

    namespace Message
    {
        /* Return codes messages */
        const std::string OK =  "OK";
        const std::string WRONG_AUTH = "Wrong authentication";
        const std::string INCOMPLETE_SET = "Incomplete set";
        const std::string NO_SUCH_SPEC = "No such specifier";
        const std::string TOO_MUCH_DATA = "Too much data";
        const std::string NO_SUCH_SET = "No such set";
        const std::string WRONG_TYPE = "Wrong type";
        const std::string UNKNOWN_NAME = "Unknown name";
        const std::string UNKNOWN_FIELD = "Unknown field";
        const std::string GENERIC_ERROR = "Generic error";
    }
}

#endif
