/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef UTILS_H
#define UTILS_H

#include <string>

/** @namespace utils A namespace with many utility functions, both for UNIX and Win32. */
namespace Utils
{
    const int ERROR_BUFF_SIZE = 2048;           /** Max char allowed in the error message buffer. */
    const size_t KILOBYTE = 1000;               /** Constat to convert Bytes into Kilobytes. */
    const size_t MEGABYTE = 1000000;            /** Costant to convert Bytes into Megabytes. */
    
   /**
    * @param s A generic string
    * @return true if the string has only spaces, false otherwise.
    */
    bool isBlankLine(const std::string& s); 
    
   /**
    * @param s A generic string
    * @return true if the string has only numerical digits, false otherwise.
    */
    bool isIntegerString(const std::string& s);

   /**
    * @param s A generic string
    * @return true if the string has only numerical digits and exactly one "point", false otherwise.
    */
    bool isFloatString(const std::string& s);
    
   /**
    * @param s A generic string
    * @return true if the string is a single quoted word (like 'apple'), false otherwise.
    */  
    bool isQuoted(const std::string& s);
    
   /**
    * @param s A generic string
    * @return true if the string is a single quoted sentence (like 'a sentence'), false otherwise.
    */  
    bool isQuotedSentence(const std::string& s);
    
   /**
    * @param str A generic string
    * @param substr Another generic string, supposed to be a substring of str.
    * @return the string <str> without the substring <substr>, if <substr> is a substring of <str>; otherwise the original string <str>.
    */
    std::string removeSubstring(const std::string& str, const std::string substr);
    
   /**
    * @param str A generic string
    * @return The given string without its last character, if this one is a slash or a backslash; otherwise the original string.
    */
    std::string removeEndSlash(const std::string& str);
    
   /**
    * Extract words from the given stream until a single quote is found, to create a sentence.
    * @param parser The input stream to be parsed
    * @param begin A generic string which will be used as the start of the sentence. It should start with a single quote.
    * @return A single quoted sentence extracted from the given stream.
    */
    std::string parseUntilQuote(std::istringstream& parser, const std::string& begin);
    
   /**
    * @param type A generic object to be converted into a string.
    * @return The string conversion of the given object.
    */
    template<typename T> std::string toString(T type);
    
   /**
    * Create a well-formatted string explaining the given error code returned by a failed system call.
    * @param syscall The name of the failed system call.
    * @param errorCode The error code returned by the failed system call.
    * @return A string with the form: <syscall> failed [error <errorCode>]: <platform-specific error message>
    */
    std::string syscallError(const std::string& syscall, int errorCode);

   /**
    * @return String representation of the current datetime, in the format dd/mm/yy hh:mm:ss.
    */
    std::string getCurrentTime();
    
   /**
    * @param size A certain byte size.
    * @return String with the form "n B/KB/MB", depending on how many bytes has been provided in the argument.
    */
    std::string formatSize(size_t size);
    
   /**
    * @param errorCode An error code (POSIX or Win32)
    * @return String representation of the macro name related to the given error code. If no match are found, the string conversion of the given error code.
    */
    std::string getMacroName(int errorCode);

}

#endif
