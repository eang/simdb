/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <cctype>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <sstream>

#include "utils.h"

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))
#include <windows.h>
#else
#include <cerrno>
#include <cstring>   // required by strerror_r()
#endif

using std::istringstream;
using std::ostringstream;
using std::setprecision;
using std::string;

bool Utils::isBlankLine(const string& s)
{
    for (string::const_iterator it = s.begin(); it != s.end(); it++)
    {
        if (*it != ' ')
        {
            return false;
        }
    }
    
    return true;
}

bool Utils::isIntegerString(const string& s)
{
    if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+')))
        return false ;

    char *c;
    strtol(s.c_str(), &c, 10) ;

    /* strtol() puts in c the first char that could not be converted to an integer. */
    return (*c == 0);
}

bool Utils::isFloatString(const string& s)
{
    if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+')))
    return false ;

    char *c;
    strtod(s.c_str(), &c) ;

    /* strtod() puts in c the first char that could not be converted to a float. */
    return (*c == 0);
}

bool Utils::isQuoted(const string& s)
{
    return '\'' == s[0] && s[0] == s[s.length() - 1];
}

bool Utils::isQuotedSentence(const string& s)
{
    if (isQuoted(s))
        return s.find(' ') != string::npos;   // a sentence has at least one space

    return false;
}

string Utils::removeSubstring(const string& str, const string substr)
{
    size_t startPosition = str.find(substr);
    
    if (string::npos == startPosition)
        return str;
    
    string temp = str;
    temp.erase(startPosition, substr.length());
    return temp;
}


string Utils::removeEndSlash(const string& str)
{
    if (str.empty())
        return str;
    
    string temp = str;
    char lastChar = temp[temp.length() - 1];
    
    if ('/' == lastChar || '\\' == lastChar)
        temp.erase(temp.length() - 1);          // remove last character
        
    return temp;
}


string Utils::parseUntilQuote(istringstream& parser, const string& begin)
{
    string temp = begin;
    string word;
    
    parser >> word;
    
    while (word.find("'") == string::npos && !parser.eof())  // parse until single quote or EOF
    {
        temp += " " + word;  // build the sentence
        parser >> word;
    }
    
    temp += " " + word;
    
    return temp;
}



template<typename T> 
string Utils::toString(T type) 
{
    ostringstream stream;
    stream << setprecision(3) << type;  // setprecision if the type is float
    return stream.str();    
}

/* Template specializations for the required types */
template string Utils::toString<int>(int type);     
template string Utils::toString<float>(float type);
template string Utils::toString<size_t>(size_t type);
template string Utils::toString<long long>(long long type);


string Utils::syscallError(const std::string& syscall, int errorCode)
{
    char buffer[Utils::ERROR_BUFF_SIZE];
    ostringstream stream;
    
#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

    DWORD bytes = FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorCode, 0, (LPSTR) buffer, ERROR_BUFF_SIZE, NULL );

    if (!bytes)     // FormatMessage return 0 (written bytes) if it fails
        stream << "FormatMessage() failed";

    else
        stream << syscall << " failed [error " << getMacroName(errorCode) << "]: " << buffer;

#else

    stream << syscall << " failed [error " << getMacroName(errorCode) << "]: " << strerror_r(errorCode, buffer, Utils::ERROR_BUFF_SIZE);

#endif

    return stream.str();
}

string Utils::getCurrentTime()
{
    time_t t;
    const int buffSize = 64;
    char buff[buffSize];

    time(&t);

    /* On Win32 localtime() should be thread safe, since it's part of the libc.
       Microsoft provides also localtime_s() but doesn't seem to work with MinGW.
       On POSIX localtime() is not thread-safe, so we use localtime_r */

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

    struct tm *timeInfo = localtime(&t);
    strftime(buff, buffSize, "%d-%m-%Y %X", timeInfo);

#else

    struct tm timeInfo;
    localtime_r(&t, &timeInfo);
    strftime(buff, buffSize, "%d-%m-%Y %X", &timeInfo);

#endif

    return string(buff);
}


string Utils::formatSize(size_t size)
{
    string formattedSize;

    if (size < KILOBYTE)
    {
        formattedSize = toString(size) + " B";
    }
    
    else if (size >= KILOBYTE && size < MEGABYTE)
    {
        formattedSize = toString<float>((float) size / KILOBYTE) + " KB";
    }
    
    else if (size >= MEGABYTE)
    {
        formattedSize = toString<float>((float) size / MEGABYTE) + " MB";
    }
    
    return formattedSize;
}


string Utils::getMacroName(int errorCode)
{    
#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

    switch (errorCode)
    {
        case ERROR_FILE_NOT_FOUND: 
            return "ERROR_FILE_NOT_FOUND";
        case WSAEADDRINUSE:
            return "WSAEADDRINUSE";
        case WSAETIMEDOUT:
            return "WSAETIMEDOUT";
        case WSAECONNREFUSED:
            return "WSAECONNREFUSED";
        default:
            return toString(errorCode);
    }   
    
#else
    switch (errorCode)
    {
        case ENOENT:
            return "ENOENT";
        case EACCES:
            return "EACCES";
        case EADDRINUSE:
            return "EADDRINUSE";
        case ETIMEDOUT:
            return "ETIMEDOUT";
        case ECONNREFUSED:
            return "ECONNREFUSED";
        case EHOSTUNREACH:
            return "EHOSTUNREACH";
        default:
            return toString(errorCode);
    }
    
#endif
}




