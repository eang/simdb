/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "simdb/client.h"

using std::cerr;
using std::cout;
using std::endl;
using std::map;
using std::string;
using std::vector;

namespace
{
    const string INSERT_CMD_SHORT = "-i";
    const string INSERT_CMD_LONG = "--insert";
    const string REMOVE_CMD_SHORT = "-r";
    const string REMOVE_CMD_LONG = "--remove";
    const string GET_CMD_SHORT = "-g";
    const string GET_CMD_LONG = "--get";
    const string SEARCH_CMD_SHORT = "-s";
    const string SEARCH_CMD_LONG = "--search";
    const string SPECLIST_CMD_SHORT = "-l";
    const string SPECLIST_CMD_LONG = "--list";
    const string CMD_SHORT = "-c";
    const string CMD_LONG = "--cmd";
    
    const string ADDR_OPT_SHORT = "-a";
    const string ADDR_OPT_LONG = "--address";
    const string PORT_OPT_SHORT = "-p";
    const string PORT_OPT_LONG = "--port";
    const string DATA_OPT_SHORT = "-d";
    const string DATA_OPT_LONG = "--data";
    const string VERSION_OPT_SHORT = "-v";
    const string VERSION_OPT_LONG = "--version";
    const string HELP_OPT_SHORT = "-h";
    const string HELP_OPT_LONG = "--help";
    
    const string DI_MARKER = "DI";
    
    const string VERSION = "1.0.0";
}

/** Print program usage. */
void printUsage(const string& program);

/** Check the given arguments to find options. */
void checkOptions(vector<string>& arg, Simdb::Client& client);

/** Check the given arguments to find the command to execute. */
void checkCommands(vector<string>& arg, Simdb::Client& client);

int main(int argc, char **argv)
{
    Simdb::Client client;
    
    vector<string> arg {argv, argv + argc};
        
    checkOptions(arg, client);
    checkCommands(arg, client);

    return EXIT_SUCCESS;
}

void printUsage(const string& program)
{
    cout << "Usage: " << program << " [options] <command>" << endl;
    cout << "\nCommands:\n\n\t";
    
    cout << INSERT_CMD_SHORT << "\t dss [fields] DI items\n\t" << INSERT_CMD_LONG << " dss [fields] DI items\n\t\t";
    cout << "Insert a new DataSet in the specified <dss> (the DI marker is mandatory)\n\n\t";
    
    cout << REMOVE_CMD_SHORT << "\t sn dss\n\t" << REMOVE_CMD_LONG << " sn dss\n\t\t";
    cout << "Remove the DataSet with sequence number <sn> from the specified <dss> collection\n\n\t";   
    
    cout << GET_CMD_SHORT << "    sn dss\n\t" << GET_CMD_LONG << " sn dss\n\t\t";
    cout << "Get the DataSet with sequence number <sn> from the specified <dss> collection\n\n\t";  
    
    cout << SEARCH_CMD_SHORT << "\t dss [fields]\n\t" << SEARCH_CMD_LONG << " dss [fields]\n\t\t";
    cout << "Search in the specified <dss> collection all the DataSet matching the query\n\n\t";
    
    cout << SPECLIST_CMD_SHORT << "\n\t" << SPECLIST_CMD_LONG << "\n\t\t";
    cout << "List all the available data set specifiers\n";

    cout << "Options:\n\n\t";
    cout << ADDR_OPT_SHORT << "\t  addr\n\t" << ADDR_OPT_LONG << " addr \n\t\t";
    cout << " Set <addr> as the server IPv4 address for the TCP connection (default one is 127.0.0.1)\n\n\t";   
    
    cout << PORT_OPT_SHORT << "\tport\n\t" << PORT_OPT_LONG << "\tport \n\t\t";
    cout << "Set <port> as the default port for the TCP connection (default one is 4444)\n\n\t";        
    
    cout << DATA_OPT_SHORT << "\tdatadir \n\t" << DATA_OPT_LONG << "\tdatadir \n\t\t";
    cout << "Set <datadir> as the directory for received files storage (default one is the cwd)\n\n\t";     

    cout << VERSION_OPT_SHORT << "\n\t" << VERSION_OPT_LONG << "\n\t\t";
    cout << "Prints the client version number and then exits\n";            

    cout << HELP_OPT_SHORT << "\n\t" << HELP_OPT_LONG << "\n\t\t";
    cout << "Prints this help message and then exits\n";            
}


void checkOptions(vector<string>& arg, Simdb::Client& client)
{
    if (arg.size() == 1)
    {
        printUsage(arg[0]);
        exit(EXIT_SUCCESS);
    }
    
    auto longIt = arg.end();
    auto shortIt = find(arg.begin(), arg.end(), HELP_OPT_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), HELP_OPT_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        printUsage(arg[0]);
        exit(EXIT_SUCCESS);
    }
    
    shortIt = find(arg.begin(), arg.end(), VERSION_OPT_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), VERSION_OPT_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        cout << "SimDB client: version " << VERSION << endl;
        cout << "Using:" << endl;
        cout << "* SimDB library: version " << Simdb::LIBRARY_VERSION << endl;
        cout << "* SimDB protocol: version " << Simdb::PROTOCOL_VERSION << endl;
        exit(EXIT_SUCCESS);
    }
    
    shortIt = find(arg.begin(), arg.end(), ADDR_OPT_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), ADDR_OPT_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        auto it = shortIt != arg.end() ? shortIt : longIt;
        
        if ((it + 1) != arg.end())
        {
            client.setServerAddress(*(++it));
        }
        else
        {
            cerr << "Empty address, please insert a valid (dotted) IPv4 address." << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE);
        }   
    }
    
    
    shortIt = find(arg.begin(), arg.end(), PORT_OPT_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), PORT_OPT_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        auto it = shortIt != arg.end() ? shortIt : longIt;

        if ((it + 1) != arg.end())
        {
            if (!client.setServerPort(*(++it)))
            {
                cerr << *it << " is an invalid port number, insert an integer >= 1024" << endl;
                exit(EXIT_FAILURE);
            }
        }

        else
        {
            cerr << "Empty port, please insert a valid port number." << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE);
        }
    }
    
    
    shortIt = find(arg.begin(), arg.end(), DATA_OPT_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), DATA_OPT_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        auto it = shortIt != arg.end() ? shortIt : longIt;

        if ((it + 1) != arg.end())
        {
            client.setDataDirectory(*(++it));
        }

        else
        {
            cerr << "Empty data directory, please insert a directory to store data files." << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE);
        }
    }
}

void checkCommands(vector<string>& arg, Simdb::Client& client)
{
    bool found = false;     // flag to store whether at least one command has been found
    
    auto shortIt = arg.end();
    auto longIt = arg.end();
    
    shortIt = find(arg.begin(), arg.end(), CMD_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), CMD_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        auto it = shortIt != arg.end() ? shortIt : longIt;
        found = true;

        if ((it + 1) != arg.end())
        {
            string command = *(++it);
            client.sendExtendedCommand(command);
        }

        else
        {
            cerr << "empty command." << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE);
        }
    }
    
    shortIt = find(arg.begin(), arg.end(), INSERT_CMD_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), INSERT_CMD_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        auto it = shortIt != arg.end() ? shortIt : longIt;
        found = true;
        
        string dssName, temp1, temp2;
        map<string, string> sdFields;
        map<string, string> dataItems;

        if ((it+ 1) != arg.end() && *(it+1) != DI_MARKER)
            dssName = *(++it);
        
        else
        {
            cerr << "Missing Data Set Specifier name" << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE);
        }
        
        if (find(arg.begin(), arg.end(), DI_MARKER) == arg.end())
        {
            cerr << "Missing DI marker" << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE); 
        }
        
        while ((it+1) != arg.end() && (it+2) != arg.end() && *(it+1) != DI_MARKER && *(it+2) != DI_MARKER)
        {
            temp1 = *(++it);
            temp2 = *(++it);
            sdFields.insert({temp1, temp2});
        }
        
        it++;
        
        while ((it+1) != arg.end() && (it+2) != arg.end())
        {
            temp1 = *(++it);
            temp2 = *(++it);
            dataItems.insert({temp1, temp2});
        }
        
        client.insertDataSet(dssName, sdFields, dataItems);
    }   
    

    shortIt = find(arg.begin(), arg.end(), REMOVE_CMD_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), REMOVE_CMD_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        auto it = shortIt != arg.end() ? shortIt : longIt;
        found = true;

        if ((it + 1) != arg.end() && (it + 2) != arg.end())
        {
            client.removeDataSet(*(it + 1), *(it + 2));
        }

        else
        {
            cerr << "Incomplete remove command." << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE);
        }
    }   
    
    shortIt = find(arg.begin(), arg.end(), GET_CMD_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), GET_CMD_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        auto it = shortIt != arg.end() ? shortIt : longIt;
        found = true;

        if ((it + 1) != arg.end() && (it + 2) != arg.end())
        {
            client.getDataSet(*(it + 1), *(it + 2));
        }

        else
        {
            cerr << "Incomplete get command." << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE);
        }
    }       
    
    
    shortIt = find(arg.begin(), arg.end(), SEARCH_CMD_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), SEARCH_CMD_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        auto it = shortIt != arg.end() ? shortIt : longIt;
        found = true;
        
        string dssName, temp1, temp2;
        map<string, string> sdQueryFields;

        if ((it + 1) != arg.end())
            dssName = *(++it);
        
        else
        {
            cerr << "Missing Data Set Specifier name" << endl;
            printUsage(arg[0]);
            exit(EXIT_FAILURE);
        }
        

        while ((it+1) != arg.end() && (it+2) != arg.end())
        {
            temp1 = *(++it);
            temp2 = *(++it);
            sdQueryFields.insert({temp1, temp2});
        }
        
        client.searchDataSet(dssName, sdQueryFields);
    }
    
    
    shortIt = find(arg.begin(), arg.end(), SPECLIST_CMD_SHORT);
    
    if (shortIt == arg.end())
        longIt = find(arg.begin(), arg.end(), SPECLIST_CMD_LONG);
    
    if (shortIt != arg.end() || longIt != arg.end())
    {
        found = true;
        client.listDss();
    }
    
    if (!found)
    {
        cerr << "Missing mandatory command, please insert a valid command." << endl;
        printUsage(arg[0]);
    }
}

