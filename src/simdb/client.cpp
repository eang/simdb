/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifdef DEBUG_MODE
#define DEBUG(x) do { std::cerr << "DEBUG: " << x << std::endl; } while (0)
#else
#define DEBUG(x) do { ; } while (0)
#endif

#include <cstdlib>
#include <fstream>
#include <sstream>

#include <b64/encode.h>
#include <b64/decode.h>

#include "client.h"
#include "net/clienthandler.h"
#include "protocol/protocol.h"
#include "utils/utils.h"
#include "filesystem/filesystem.h"

using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;
using std::ios;
using std::ios_base;
using std::istringstream;
using std::map;
using std::ofstream;
using std::ostringstream;
using std::string;

using Net::ClientHandler;
using Protocol::GetResponse;
using Protocol::Handler;
using Protocol::InsertResponse;
using Protocol::ResponseCode;
using Protocol::ResponseMessage;
using Protocol::SearchResponse;
using Protocol::SpeclistResponse;

using Simdb::Client;

namespace
{
    const string DEFAULT_ADDRESS = "127.0.0.1";
    const string DEFAULT_DATA_DIR = ".";
}

Client::Client() :
    serverAddress {DEFAULT_ADDRESS},
    serverPort {DEFAULT_PORT},
    dataDirectory {DEFAULT_DATA_DIR}
{}



bool Client::insertDataSet(const string& dssName, map<string, string>& sdFields, const map<string, string>& dataItems)
{
    printInitialMessage();
    string request;

    try
    {
        request = Handler::createInsertRequest(dssName, sdFields, dataItems);
    }
    catch (const Filesystem::Error& e)
    {
        cerr << "Filesystem error: " << e.what() << endl;
        cerr << "Check pathnames and/or command syntax" << endl;
        return false;
    }
    
    DEBUG(request);
    
    return insertDataSet(request, dataItems);
}



bool Client::removeDataSet(const string& sequenceNumber, const string& dssName)
{
    printInitialMessage();
    
    string request = Handler::createRemoveRequest(sequenceNumber, dssName);
    
    DEBUG(request);
    
    return removeDataSet(request);
}

bool Client::getDataSet(const string& sequenceNumber, const string& dssName)
{
    printInitialMessage();
    
    string request = Handler::createGetRequest(sequenceNumber, dssName);
    
    DEBUG(request);
    
    return getDataSet(request);
}

bool Client::searchDataSet(const string& dssName, map<string, string>& sdQueryFields)
{
    printInitialMessage();
    
    string request = Handler::createSearchRequest(dssName, sdQueryFields);
    
    DEBUG(request);

    return searchDataSet(request);
}



bool Client::listDss()
{
    printInitialMessage();
    
    ClientHandler clientHandler {serverAddress, serverPort};

    if (clientHandler.createSocket() && clientHandler.startConnection())
    {       
        clientHandler.sendMessage(Protocol::Syntax::SPECLIST);
        string responseMessage = clientHandler.receiveMessage();
                
        try
        {
            auto speclistResponse = Handler::parseSpeclistResponse(responseMessage);
            cout << speclistResponse << endl;
        }
        catch (const Protocol::Syntax::Error& e)
        {
            cerr << "Protocol syntax error: " << e.what() << endl;
        }
    }
    else 
    {
        cerr << "Failed to connect to " << serverAddress << ":" << serverPort << ", check address and/or port" << endl;
        return false;
    }

    cout << "Connection closed by server." << endl;
    clientHandler.closeConnection();
    return true;
}



bool Client::sendExtendedCommand(const string& command)
{
    printInitialMessage();

    try
    {
        if (command.find(Protocol::Syntax::INSERT) != string::npos)
        {
            map<string, string> dataItems;
            string cleanedRequest = Handler::createInsertRequest(command, dataItems);
            DEBUG(cleanedRequest);
            return insertDataSet(cleanedRequest, dataItems);
        }
        
        if (command.find(Protocol::Syntax::REMOVE) != string::npos)
        {
            string cleanedRequest = Handler::createRemoveRequest(command);
            DEBUG(cleanedRequest);
            return removeDataSet(cleanedRequest);
        }
        
        if (command.find(Protocol::Syntax::SEARCH) != string::npos)
        {
            string cleanedRequest = Handler::createSearchRequest(command);
            DEBUG(cleanedRequest);
            return searchDataSet(cleanedRequest);
        }
            
        if (command.find(Protocol::Syntax::GET) != string::npos)
        {
            string cleanedRequest = Handler::createGetRequest(command);
            DEBUG(cleanedRequest);
            return getDataSet(cleanedRequest);
        }
        
        if (command.find(Protocol::Syntax::SPECLIST) != string::npos)
            return listDss();

        cout << "Invalid command name." << endl;
        return false;
    }
    catch (const Protocol::Syntax::Error& e)
    {
        cerr << "Protocol syntax error: " << e.what() << endl;
        return false;
    }
    catch (const Filesystem::Error& e)
    {
        cerr << "Filesystem error: " << e.what() << endl;
        cerr << "Check pathnames and/or command syntax";
        return false;
    }
}

void Client::setServerAddress(const string& address)
{
    serverAddress = address;
}

bool Client::setServerPort(const string& port)
{
    int n = atoi(port.c_str());
    
    if (n < Net::VALID_PORT_NUMBER)
    {
        return false;
    }
    
    serverPort = n;
    return true;
}

void Client::setDataDirectory(const string& dataDir)
{
    dataDirectory = Utils::removeEndSlash(dataDir);     // we don't want the ending slash, it will be added when necessary

    if (!Filesystem::existDirectory(dataDir))
        Filesystem::createDirectory(dataDir);
}


/* Private functions to handle the commands */


bool Client::insertDataSet(const string& message, const map<string, string>& dataItems)
{
    ClientHandler clientHandler(serverAddress, serverPort);

    try
    {       
        if (clientHandler.createSocket() && clientHandler.startConnection())
        {
            clientHandler.sendMessage(message);
            string risp = clientHandler.receiveMessage();
            ResponseMessage response = Handler::parseResponseMessage(risp);
                
            if (response.getResponseCode() != ResponseCode::Ok)
            {
                cout << response << endl;
                cerr << Handler::getExtendedErrorMessage(response.getResponseCode()) << endl;
                clientHandler.closeConnection();
                return false;
            }

            base64::encoder enc;
            string itemName, fileName;

            for (map<string, string>::const_iterator itemIt = dataItems.begin(); itemIt != dataItems.end(); itemIt++)
            {
                itemName = itemIt->first;
                fileName = itemIt->second;

                ifstream input {fileName.c_str(), ios_base::in | ios_base::binary};  // binary mode to prevent that \n is replaced by \r\n on Windows.
                ostringstream output;

                enc.encode(input, output);
                string encodedFile = output.str();  // it could be a huge string...
                    
                clientHandler.sendMessage(itemName + ' ' + Utils::toString(encodedFile.length()) + '\n');  // send item name and the encoded size of its related file
                    
                risp = clientHandler.receiveMessage();      // wait for the ACK to start the file sending
                
                if (risp != Protocol::Syntax::FILE_ACK)
                {
                    response = Handler::parseResponseMessage(risp);
                    cout << response << endl;
                    cerr << Handler::getExtendedErrorMessage(response.getResponseCode()) << endl;
                    clientHandler.closeConnection();
                    return false;
                }

                cout << "Sending file " << fileName  << " (encoded size: " << Utils::formatSize(encodedFile.length()) << ")..." << endl;
                clientHandler.sendMessage(encodedFile);         // send encoded file
    
                risp = clientHandler.receiveMessage();
                response = Handler::parseResponseMessage(risp);
                    
                if (response.getResponseCode() != ResponseCode::Ok)
                {
                    cout << response << endl;
                    cerr << Handler::getExtendedErrorMessage(response.getResponseCode()) << endl;
                    clientHandler.closeConnection();
                    return false;
                }
            }

            risp = clientHandler.receiveMessage();  
            auto insertResponse = Handler::parseInsertResponse(risp);  // final message with the SN

            cout << insertResponse << endl;
                
            if (insertResponse.getResponseCode() != ResponseCode::Ok)
            {
                cerr << Handler::getExtendedErrorMessage(response.getResponseCode()) << endl;
                clientHandler.closeConnection();
                return false;
            }
        }

        else 
        {
            cerr << "Failed to connect to " << serverAddress << ":" << serverPort << ", check address and/or port" << endl;
            return false;
        }
            
    }
    catch (const Protocol::Syntax::Error& e)
    {
        cerr << "Syntax error: " << e.what() << endl;
        clientHandler.closeConnection();
        return false;
    }
    catch (const Filesystem::Error& e)
    {
        cerr << "Filesystem error, check pathnames: " << e.what() << endl;
        return false;
    }

    cout << "Connection closed by server." << endl;
    clientHandler.closeConnection();
    return true;
}


bool Client::removeDataSet(const std::string& command)
{
    ClientHandler clientHandler {serverAddress, serverPort};

    if (clientHandler.createSocket() && clientHandler.startConnection())
    {
        clientHandler.sendMessage(command);
        string risp = clientHandler.receiveMessage();
                
        try
        {
            auto responseMessage = Handler::parseResponseMessage(risp);

            cout << responseMessage << endl;

            if (responseMessage.getResponseCode() != ResponseCode::Ok)
            {
                cerr << Handler::getExtendedErrorMessage(responseMessage.getResponseCode()) << endl;
                clientHandler.closeConnection();
                return false;
            }
        }
        catch (const Protocol::Syntax::Error& e)
        {
            cerr << "Protocol syntax error: " << e.what() << endl;
        }
    }
    
    else
    {
        cerr << "Failed to connect to " << serverAddress << ":" << serverPort << ", check address and/or port" << endl;
        return false;
    }

    cout << "Connection closed by server." << endl;
    clientHandler.closeConnection();
    return true;
}

bool Client::getDataSet(const std::string& command)
{
    ClientHandler clientHandler {serverAddress, serverPort};

    if (clientHandler.createSocket() && clientHandler.startConnection())
    {
        clientHandler.sendMessage(command);
        string risp = clientHandler.receiveMessage();
        DEBUG(risp);
                
        try
        {
            auto getResponse = Handler::parseGetResponse(risp);
            cout << getResponse << endl;

            if (getResponse.getResponseCode() != ResponseCode::Ok)
            {
                cerr << Handler::getExtendedErrorMessage(getResponse.getResponseCode()) << endl;
                clientHandler.closeConnection();
                return false;
            }

            clientHandler.sendMessage(Protocol::Syntax::FILE_ACK);   // send the first ACK, to make start the file sendind
            
            map<string, string>& dataItems = getResponse.getDataItems();
            
            for (int i = 0; i < getResponse.getFilesNumber(); i++)
            {
                DEBUG("Waiting file header...");
                string fileInfo = clientHandler.receiveMessage();   // receive the header "itemName encodedSize"
                
                istringstream parser {fileInfo};
                string itemName;
                size_t encodedSize;
                parser >> itemName >> encodedSize;

                cout << "Receiving file " << dataItems[itemName] << " (" << Utils::formatSize(encodedSize) << ")..." << endl;

                clientHandler.sendMessage(Protocol::Syntax::FILE_ACK);   // send the ACK for the header to make start the file sending
                
                char *encodedFile = clientHandler.receiveFile();

                if (encodedFile == nullptr)
                {
                    cerr << "Failed receiving of file " << itemName << endl;
                    clientHandler.closeConnection();
                    return false;
                }
            
                string encoding {encodedFile};
                string storePath = dataDirectory + "/" + dataItems[itemName];  
                
                istringstream input {encoding, ios::in | ios::binary};
                ofstream output {storePath.c_str(), ios::out | ios::binary};

                if (!output.is_open())
                {
                    cerr << "Cannot save file in " << storePath << endl;
                    clientHandler.closeConnection();
                    return false;
                }


                base64::decoder dec;
                dec.decode(input, output);
                
                output.close();
                
                if (DEFAULT_DATA_DIR == dataDirectory)
                    storePath = Filesystem::getCurrentDirectory() + "/" + dataItems[itemName];
                
                cout << "--> File saved in " << storePath << endl;  
                
                clientHandler.sendMessage(Protocol::Syntax::FILE_ACK);   // send the ACK also when the file is received, it's very important.
            }
        }
        catch (const Protocol::Syntax::Error& e)
        {
            cerr << "Protocol syntax error: " << e.what() << endl;
        }
    }
    
    else
    {
        cerr << "Failed to connect to " << serverAddress << ":" << serverPort << ", check address and/or port" << endl;
        return false;
    }

    cout << "\nAll files received successfully." << endl;
    cout << "Connection closed by server." << endl;
    clientHandler.closeConnection();
    return true;
}

bool Client::searchDataSet(const std::string& message)
{
    ClientHandler clientHandler {serverAddress, serverPort};

    if (clientHandler.createSocket() && clientHandler.startConnection())
    {
        clientHandler.sendMessage(message);
        
        string risp = clientHandler.receiveMessage();
        DEBUG(risp);
        auto searchResponse = Handler::parseSearchResponse(risp);

        cout << searchResponse << endl;

        if (searchResponse.getResponseCode() != ResponseCode::Ok)
        {
            cerr << Handler::getExtendedErrorMessage(searchResponse.getResponseCode()) << endl;
            clientHandler.closeConnection();
            return false;
        }
    }
    
    else
    {
        cerr << "Failed to connect to " << serverAddress << ":" << serverPort << ", check address and/or port" << endl;
        return false;
    }

    cout << "Connection closed by server." << endl;
    clientHandler.closeConnection();
    return true;
}

void Client::printInitialMessage()
{
    cout << "*** SimDB Client ***" << endl;
}


