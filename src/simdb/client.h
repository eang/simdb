/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef CLIENT_H
#define CLIENT_H

#include <map>
#include <string>

#include "net/net.h"
#include "protocol/clientprotocol.h"
#include "simdb.h"

namespace Simdb
{
    
   /**
    * @brief API for external clients.
    * This class provides a client-side API for the SimDB system. It provides functions that a generic C++ program can use to comunicate as a client with a SimDB server.
    */
    class Client
    {

    public:

        Client();

       /**
        * Send an INSERT command to the server.
        * @param dssName The DSS name in the command.
        * @param sdFields The list of SetDescriptor fields, as a <key, value> map.
        * @param dataItems The list of DataItems and their instance files, as an <item, instance> map.
        * @return true if the operation succeeds, false otherwise.
        */
        bool insertDataSet(const std::string& dssName, std::map<std::string, std::string>& sdFields, const std::map<std::string, std::string>& dataItems);

       /**
        * Send a REMOVE command to the server.
        * @param sequenceNumber The SN of the DataSet to be removed.
        * @param dssName The DSS name in the command.
        * @return true if the operation succeeds, false otherwise.
        */
        bool removeDataSet(const std::string& sequenceNumber, const std::string& dssName);

       /**
        * Send a GET command to the server. The received data is saved in the data directory.
        * @param sequenceNumber The SN of the DataSet to be retrieved.
        * @param dssName The DSS name in the command.
        * @return true if the operation succeeds, false otherwise.
        */
        bool getDataSet(const std::string& sequenceNumber, const std::string& dssName);

       /**
        * Send a SEARCH command to the server.
        * @param dssName The DSS name in the command.
        * @param sdQueryFields The list of SetDescriptor fields in the query, as a <key, value> map. It can be empty.
        * @return true if the operation succeeds, false otherwise.
        */
        bool searchDataSet(const std::string& dssName, std::map<std::string, std::string>& sdQueryFields);

       /**
        * Send a SPECLIST command to the server. 
        * @return true if the operation succeeds, false otherwise.
        */
        bool listDss();

       /**
        * Send an extended command to the server. The command is parsed to understand its type.
        * @param commands The string with the command message to be sent.
        * @return true if the operation succeeds, false otherwise.
        */
        bool sendExtendedCommand(const std::string& command);
        

       /**
        * Set a custom server address different from the default one.
        * @param address A custom IPv4 server address in the "dotted" format (e.g. "192.168.0.1").
        */
        void setServerAddress(const std::string& address);
        
       /**
        * Set a custom server port different from the default one.
        * The given string is converted into an unsigned short.
        * @param port A custom server port; it must be an integer greather than 1024, while integers greather than 65535 are treated as 65535.
        * @return true if the port is valid, false otherwise.
        */
        bool setServerPort(const std::string& port);
        
       /**
        * Set a custom data directory different from the default one.
        * If the given directory doesn't exists it is created.
        * @param dataDir An absolute directory path.
        */
        void setDataDirectory(const std::string& dataDir);
        
    private:

        static const Net::Port DEFAULT_PORT = 4444;

        std::string serverAddress;              /** Server IPv4 address. */
        Net::Port serverPort;                   /** Server port. */
        std::string dataDirectory;              /** Directory where the received files are stored. */
        
        bool insertDataSet(const std::string& message, const std::map<std::string, std::string>& dataItems);
        bool removeDataSet(const std::string& command);
        bool getDataSet(const std::string& command);
        bool searchDataSet(const std::string& message);
        
        /** Print a simple "presentation" message. */
        void printInitialMessage();
    };
}





#endif
