/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <cstdlib>
#include <cstring>
#include <iostream>

#include "clienthandler.h"
#include "utils/utils.h"

using std::cerr;
using std::cout;
using std::endl;
using std::string;

using Net::ClientHandler;

ClientHandler::ClientHandler(const std::string& address, Port port) :
    SocketHandler {port},
    socketDescriptor{DEFAULT_SOCKET_DESCRIPTOR},
    serverAddress{address}
{
    setupSocketAddress();
}

bool ClientHandler::createSocket()
{
    if (currentState != SocketState::Inactive)
        return false;   // we create a new socket only in the inactive state

    int s = socket(AF_INET, SOCK_STREAM, 0);

    if (s != GENERIC_SOCKET_ERROR)
    {
        socketDescriptor = s;
        currentState = SocketState::Ready;

        return true;
    }

    cerr << Utils::syscallError("socket()", getLastErrorCode()) << endl;

    return false;
}

bool ClientHandler::startConnection()
{
    if (currentState != SocketState::Ready)
        return false;

    cout << "Connecting to " << serverAddress << ":" << serverPort << "..." << endl;
    
    if (connect(socketDescriptor, (sockaddr *) &socketAddress, sizeof(sockaddr_in)) != GENERIC_SOCKET_ERROR)
    {
        currentState = SocketState::Connected;
        cout << "Connection established" << endl;
        return true;
    }

    cerr << Utils::syscallError("connect()", getLastErrorCode()) << endl;
    
    return false;
}

void ClientHandler::closeConnection()
{
    if (currentState != SocketState::Connected)
        return;
    
    if (closeSocket(socketDescriptor))
        currentState = SocketState::Disconnected;
    
    else
        cerr << Utils::syscallError("close()", getLastErrorCode()) << endl;
}


void ClientHandler::sendMessage(const string& message)
{
    if (currentState != SocketState::Connected)
        return;

    string closedMessage = message + END_OF_MESSAGE;    // append the ASCII ETX char as message ending char
    
    int sentBytes = send(socketDescriptor, closedMessage.c_str(), closedMessage.length(), 0);

    if (sentBytes == GENERIC_SOCKET_ERROR)
    {
        cerr << Utils::syscallError("send()", getLastErrorCode()) << endl;
    }
}



string ClientHandler::receiveMessage()
{
    if (currentState != SocketState::Connected)
        return "";

    string partialMessage, completeMessage;
    int receivedBytes = 0;
    
    do
    {
        char buffer[MESSAGE_BUFFER_SIZE + 1];  // +1 for the null byte \0
        receivedBytes = recv(socketDescriptor, buffer, MESSAGE_BUFFER_SIZE, 0);

        if (receivedBytes == 0)
            cout << "Connection closed by server" << endl;
            
        else if (receivedBytes == GENERIC_SOCKET_ERROR)
            cerr << Utils::syscallError("recv())", getLastErrorCode()) << endl;
            
        else
        {
            buffer[receivedBytes] = '\0';
            partialMessage = string(buffer);
            completeMessage += partialMessage;
        }
    }
    while (receivedBytes > 0 && partialMessage.find(END_OF_MESSAGE) == string::npos);

    return completeMessage.substr(0, completeMessage.length() - 1);   // remove the ETX ending char
}


char *ClientHandler::receiveFile()
{
    char *storeBuffer = nullptr;
    string partialFile, completeFile;
    int receivedBytes = 0;
    
    do
    {
        char buffer[MESSAGE_BUFFER_SIZE + 1];    // +1 for the null byte \0
        receivedBytes = recv(socketDescriptor, buffer, MESSAGE_BUFFER_SIZE, 0);

        if (receivedBytes == 0)
            cout << "Connection closed by server" << endl;
            
        else if (receivedBytes == GENERIC_SOCKET_ERROR)
            cerr << Utils::syscallError("recv()", getLastErrorCode()) << endl;
            
        else
        {
            buffer[receivedBytes] = '\0';
            partialFile = string(buffer);
            completeFile += partialFile;
        }
    }
    while (receivedBytes > 0 && partialFile.find(END_OF_MESSAGE) == string::npos);
    
    if (!completeFile.empty())
    {
        storeBuffer = (char *) malloc(completeFile.length());   //  the ending char ETX must be removed and we don't allocate memory for it; it will be replaced by the null byte \0
        
        if (storeBuffer)
        {
            strncpy(storeBuffer, completeFile.c_str(), completeFile.length() - 1);  // copy everything except the ETX char
            storeBuffer[completeFile.length() - 1] = '\0';
        }
    }

    return storeBuffer;
}


void ClientHandler::setupSocketAddress()
{
    auto address = inet_addr(serverAddress.c_str());

    if (address == (unsigned int) GENERIC_SOCKET_ERROR)
    {
        cerr << "inet_addr() failed: " << serverAddress << " is an invalid IPv4 address" << endl;
        currentState = SocketState::Unusable;
    }

    else
    {
        memset(&socketAddress, 0, sizeof(socketAddress));
        socketAddress.sin_family = AF_INET;
        socketAddress.sin_addr.s_addr = address;
        socketAddress.sin_port = htons(serverPort);
    }
}

