/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef CLIENT_SERVICE_H
#define CLIENT_SERVICE_H

#include "net.h"

namespace Net
{

   /**
    * @brief Base Class with basic convenience support for its derived classes.
    *
    * This class provides mainly wrapper functions to hide the differences between UNIX and Win32, such as socket closing and error codes receiving.
    * Client and server are supposed to extend this class to add their specific features.
    */
    class SocketHandler
    {

    public:

        SocketHandler(Port port);
        ~SocketHandler();

        static const int GENERIC_SOCKET_ERROR = -1;
        static const int DEFAULT_SOCKET_DESCRIPTOR = -1;
        static const Port DEFAULT_INVALID_PORT = -1;

    protected:

        static const int MESSAGE_BUFFER_SIZE = 8192;
        static const char END_OF_MESSAGE = 3;
        Port serverPort;                                /** Server port, required by both client and server sides. */
        SocketState currentState;                       /** Socket current state. */

       /**
        * Wrapper function to get the system calls error codes regardless the underlying platform.
        * @return The last error code returned by a failed system call.
        */
        int getLastErrorCode();

        /**
        * Wrapper function to close a socket regardless the underlying platform.
        * @param descriptor The socket descriptor.
        * @return true if the operation succeeds, false otherwise.
        */
        bool closeSocket(int descriptor);

    private:

        /** Wrapper for the WSAStartup() function. */
        void winsockStartup(); // TODO: #ifdef WIN here?
    };
}




#endif

