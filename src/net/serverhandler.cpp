/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifdef DEBUG_MODE
#define DEBUG(x) do { std::cerr << "DEBUG: " << x << std::endl; } while (0)
#else
#define DEBUG(x) do { ; } while (0)
#endif

#include <cstdlib>
#include <cstring>

#include "serverhandler.h"
#include "server/logs.h"
#include "utils/utils.h"

using std::endl;
using std::string;

using Logs::err;
using Logs::out;

using Net::ServerHandler;

ServerHandler::ServerHandler(Port port) :
    SocketHandler {port},
    socketDescriptor {DEFAULT_SOCKET_DESCRIPTOR}
{
    memset(&socketAddress, 0, sizeof(socketAddress));
    socketAddress.sin_family = AF_INET;
    socketAddress.sin_addr.s_addr = INADDR_ANY;  // INADDR_ANY is 0, so htonl(INADDR_ANY) is not required
    socketAddress.sin_port = htons(serverPort);
}

bool ServerHandler::createSocket()
{
    if (currentState != SocketState::Inactive)
        return false;   // we create a new socket only in the inactive state

    int s = socket(AF_INET, SOCK_STREAM, 0);

    if (s != GENERIC_SOCKET_ERROR)
    {
        socketDescriptor = s;
        
        /* On UNIX the SO_REUSEADDR option requires an int "actiovation" parameter, but Win32 requires a char, that's why the cast to char. */
        int on = 1;  
        if (setsockopt(socketDescriptor, SOL_SOCKET, SO_REUSEADDR, (const char *) &on, sizeof(on)) != GENERIC_SOCKET_ERROR)   
        {
            currentState = SocketState::Ready;
            return true;
        }
        
        err << Utils::syscallError("setsockopt()", getLastErrorCode()) << endl;
        return false;
    }

    err << Utils::syscallError("socket()", getLastErrorCode()) << endl;
    return false;
}

bool ServerHandler::startListening()
{
    if (currentState != SocketState::Ready)
        return false;

    if (bind(socketDescriptor, (sockaddr *) &socketAddress, sizeof(sockaddr_in)) != GENERIC_SOCKET_ERROR)
    {
        if (listen(socketDescriptor, BACKLOG) == GENERIC_SOCKET_ERROR)
        {
            err << Utils::syscallError("listen()", getLastErrorCode()) << endl;
            return false;
        }

        currentState = SocketState::Listening;
        return true;
    }

    err << Utils::syscallError("bind()", getLastErrorCode()) << endl;
    return false;
}

int ServerHandler::acceptConnection()
{
    if (currentState != SocketState::Listening)
        return GENERIC_SOCKET_ERROR;

    sockaddr_in remoteSocketAddress;
    size_t size = sizeof(sockaddr_in);

    int connectedSocket;

    connectedSocket = accept(socketDescriptor, (sockaddr *) &remoteSocketAddress, (socklen_t *) &size);

    if (connectedSocket == GENERIC_SOCKET_ERROR)
    {

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

        if (getLastErrorCode() != WSAEINTR)         // On Win32 when the signal-handling thread closes the listening socket, there is the WSAEINTR error but it's not an error that we want to notify
            err << Utils::syscallError("accept()", getLastErrorCode()) << endl;
        
#else
        if (getLastErrorCode() != EINTR)    // same as above, but on UNIX when the signal arrives is the main thread to be interrupted
            err << Utils::syscallError("accept()", getLastErrorCode()) << endl;

#endif
        return GENERIC_SOCKET_ERROR;
    }

    out << "Accepted connection from: " << inet_ntoa(remoteSocketAddress.sin_addr) << ":" << ntohs(remoteSocketAddress.sin_port) << endl;
    
    return connectedSocket;
}

string ServerHandler::receiveMessage(int connectedSocket, SocketState& threadState)
{
    
    if (threadState != SocketState::Connected)
        return "";

    string partialMessage, completeMessage;
    int receivedBytes = 0;
    
    do
    {
        char buffer[MESSAGE_BUFFER_SIZE + 1];  // +1 for the null byte '\0'
        receivedBytes = recv(connectedSocket, buffer, MESSAGE_BUFFER_SIZE, 0);

        if (receivedBytes == 0)
            out << "Connection closed by client" << endl;
            
        else if (receivedBytes == GENERIC_SOCKET_ERROR)
            err << Utils::syscallError("recv())", getLastErrorCode()) << endl;
            
        else  // everything is ok, the are bytes to store
        {
            buffer[receivedBytes] = '\0';
            partialMessage = string(buffer);
            completeMessage += partialMessage;
        }
    }
    while (receivedBytes > 0 && partialMessage.find(END_OF_MESSAGE) == string::npos);  // receive stuff until we find the ETX ending char

    return completeMessage.substr(0, completeMessage.length() - 1);   // remove the ETX ending char
}

void ServerHandler::sendMessage(int connectedSocket, const string& message, SocketState& threadState)
{
    if (threadState != SocketState::Connected)
        return;

    string closedMessage = message + END_OF_MESSAGE;        // append the ASCII ETX char as message ending char
    
    int sentBytes = send(connectedSocket, closedMessage.c_str(), closedMessage.length(), 0);
    
    if (sentBytes == GENERIC_SOCKET_ERROR)
    {
        err << Utils::syscallError("send()", getLastErrorCode()) << endl;
    }
}

char *ServerHandler::receiveFile(int connectedSocket, SocketState& threadState)
{
    if (threadState != SocketState::Connected)
        return nullptr;

    char *storeBuffer = nullptr;
    string partialFile, completeFile;
    int receivedBytes = 0;
    
    do
    {
        char buffer[MESSAGE_BUFFER_SIZE + 1];    // +1 for the null byte '\0'
        receivedBytes = recv(connectedSocket, buffer, MESSAGE_BUFFER_SIZE, 0);

        if (receivedBytes == 0)
            out << "Connection closed by client" << endl;
            
        else if (receivedBytes == GENERIC_SOCKET_ERROR)
            err << Utils::syscallError("recv()", getLastErrorCode()) << endl;
            
        else
        {
            buffer[receivedBytes] = '\0';
            partialFile = string(buffer);
            completeFile += partialFile;
        }
    }
    while (receivedBytes > 0 && partialFile.find(END_OF_MESSAGE) == string::npos && threadState == SocketState::Connected);
    
    if (threadState != SocketState::Connected)  // the download has been interrupted
        out << "Download interrupted" << endl;

    
    if (!completeFile.empty())
    {
        storeBuffer = (char *) malloc(completeFile.length());   // the ending char ETX must be removed and we don't allocate memory for it; it will be replaced by the null byte \0
        
        if (storeBuffer)
        {
            strncpy(storeBuffer, completeFile.c_str(), completeFile.length() - 1);  // copy everything except the ETX char
            storeBuffer[completeFile.length() - 1] = '\0';
        }
    }

    return storeBuffer;
}


void ServerHandler::closeConnection(int connectedSocket, SocketState& threadState)
{
    if (closeSocket(connectedSocket))
        threadState = SocketState::Disconnected;

    else
        err << Utils::syscallError("close()", getLastErrorCode()) << endl;
}


bool ServerHandler::stopService()
{
    return closeSocket(socketDescriptor);
}

