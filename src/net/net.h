/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef NET_H
#define NET_H

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))
/* MinGW by default doesn't define socklen_t (required by the linux-version accept()).
 * So we need to include ws2tcpip.h which requires winsock2.h instead of winsock.h
 */
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cerrno>
#endif

/** @namespace net A namespace with all the necessary stuff for the SimDB network communication. */
namespace Net
{
    enum class SocketState
    {
        Unusable,       /**< Win32 WSAStartup() has failed, the host cannot use the Windows network service. */
        Inactive,       /**< Socket not yet created. */
        Ready,          /**< Socket created and ready to be used. */
        Listening,      /**< Socket in the listening state. */
        Connected,      /**< Socket connected. */
        Disconnected    /**< Socket disconnected. The (multi-thread) server side shouldn't try to use again the thread with a new connection. */
    };

    typedef unsigned short Port;

    const int VALID_PORT_NUMBER = 1024;     /** Least port number allowed. */
}

#endif
