/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <iostream>

#include "sockethandler.h"

using std::cerr;
using std::endl;

using Net::SocketHandler;

SocketHandler::SocketHandler(Port port) :
    serverPort {port},
    currentState {SocketState::Inactive}
{

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

    winsockStartup();       // call WSAStartup()

#endif

}

SocketHandler::~SocketHandler()
{
#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

    int code = WSACleanup();
    
    switch (code)
    {
        case WSANOTINITIALISED:
            cerr << "A successful WSAStartup() call must occur before using WSACleanup()." << endl;
            break;
        case WSAENETDOWN:
            cerr << "The network subsystem has failed." << endl;
            break;
        case WSAEINPROGRESS:
            cerr << "A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function." << endl;
            break;
        default:        // 0, i.e. everything ok
            break;
    }

#endif
}


int SocketHandler::getLastErrorCode()
{

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))
    return WSAGetLastError();
#else
    return errno;
#endif

}



bool SocketHandler::closeSocket(int descriptor)
{
    int code;

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))
    
    code = closesocket(descriptor);

#else

    code = close(descriptor);

#endif

    if (code == GENERIC_SOCKET_ERROR)
    {
        return false;
    }

    return true;
}

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

void SocketHandler::winsockStartup()
{

    WSADATA wsdata;
    auto version = MAKEWORD(2, 2);      // i.e. winsock 2.2, the latest version

    int code = WSAStartup(version, &wsdata);

    switch (code)
    {
        case 0:
            currentState = SocketState::Inactive;
            break;
        case WSASYSNOTREADY:
            cerr << "Windows network subsystem is not ready for network communication." << endl;
            currentState = SocketState::Unusable;
            break;
        case WSAVERNOTSUPPORTED:
            cerr << "Version " << version << " of Windows Sockets requested is not provided by this particular implementation." << endl;
			currentState = SocketState::Unusable;
            break;
        default:
			currentState = SocketState::Unusable;
            break;
    }
}

#endif


