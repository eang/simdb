/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef CLIENT_HANDLER_H
#define CLIENT_HANDLER_H

#include <string>

#include "net.h"
#include "sockethandler.h"

namespace Net
{
   /**
    * @brief Server side of the SimDB socket interface.
    * This class provides the client side functions to simplify the connections creation and the messages sending/receiving.
    */
    class ClientHandler : public SocketHandler
    {

    public:

        ClientHandler(const std::string& address, Port port);

        /** @return true if the socket creation succeeds (through the socket() function), false otherwise. */
        bool createSocket();

        /** @return true if the connection to the server succeeds (through the connect() function), false otherwise. */
        bool startConnection();

        /** Close the connection to the server. */
        void closeConnection();

        /** @param message The message to send to the server. */
        void sendMessage(const std::string& message);

        /** @return The message received by the server. */
        std::string receiveMessage();

        /** @return Pointer to the base64-encoded file. We use a pointer for efficiency reasons. */
        char *receiveFile();

    private:

        int socketDescriptor;           /** Socket descriptor for the client communications. */
        sockaddr_in socketAddress;      /** Client main socket. */
        std::string serverAddress;      /** Server IPv4 address. */

        /** Setup the sockaddr_in struct. */
        void setupSocketAddress();
    };
}




#endif
