/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef SERVER_HANDLER_H
#define SERVER_HANDLER_H

#include <string>

#include "net.h"
#include "sockethandler.h"

namespace Net
{
   /**
    * @brief Server side of the SimDB socket interface.
    * This class provides the server side functions to simplify the incoming connections handling and the messages sending/receiving.
    */
    class ServerHandler : public SocketHandler
    {

    public:

        ServerHandler(Port port);

        /** @return true if the socket creation succeeds (through the socket() function), false otherwise. */
        bool createSocket();

        /** @return true if the socket is ready to accept connection requests (through the bind() and listen() functions), false otherwise. */
        bool startListening();

        /** @return The Socket descriptor of the connection just accepted, or the GENERIC_SOCKET_ERROR macro if something is wrong. */
        int acceptConnection();

       /** Receive a message from the client.
        * @param connectedSocket The socket whit the client connection.
        * @param threadState State of the thread which handles the connection.
        * @return String with a message send by the client.
        */
        std::string receiveMessage(int connectedSocket, SocketState& threadState);

       /**
        * @param connectedSocket The socket with the client connection.
        * @param message The message to send to the client.
        * @param threadState State of the thread which handles the connection.
        */
        void sendMessage(int connectedSocket, const std::string& message, SocketState& threadState);

       /**
        * @param connectedSocket The socket with the client connection.
        * @param threadState State of the thread which handles the connection.
        * @return Pointer to the base64-encoded file. We use a pointer for efficiency reasons. */
        char *receiveFile(int connectedSocket, SocketState& threadState);

       /**
        * Chiude una connessione con un certo client
        * @param connectedSocket The socket with the client connection.
        * @param threadState State of the thread which handles the connection.
        */
        void closeConnection(int connectedSocket, SocketState& threadState);


       /**
        * Close the listening socket and stop the server communication service.
        * @return true if the operation succeeds, false otherwise.
        */
        bool stopService();

    private:

        static const int BACKLOG = 10;  /** Backlog parameter for the listen() function. */

        int socketDescriptor;           /** Socket descriptor for the incoming connection. */
        sockaddr_in socketAddress;      /** Server main socket. */
    };
}


#endif
