/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <iostream>

#include "setdescriptor.h"
#include "protocol/protocol.h"
#include "utils/utils.h"

using std::endl;
using std::map;
using std::ostream;
using std::string;

using Entity::SetDescriptor;

SetDescriptor::SetDescriptor() :
    sequenceNumber {INVALID_SEQUENCE_NUMBER}
{}

void SetDescriptor::setSequenceNumber(int sn)
{
    sequenceNumber = sn;
}

int SetDescriptor::getSequenceNumber() const
{
    return sequenceNumber;
}

int SetDescriptor::size() const
{
    return stringFields.size() + intFields.size() + floatFields.size() + 1;  // +1 for the "extra" sequence number field
}


const map<string, string>& SetDescriptor::getStringFields() const
{
    return stringFields;
}

const map<string, int>& SetDescriptor::getIntFields() const
{
    return intFields;
}

const map<string, float>& SetDescriptor::getFloatFields() const
{
    return floatFields;
}

bool SetDescriptor::existKey(const string& key)
{
    if (!floatFields.count(key) && !intFields.count(key) && !stringFields.count(key))
        return false;

    return true;
}

bool SetDescriptor::hasValue(const string& key, const string& value)
{
    if (stringFields.count(key))
        return (stringFields[key] == value);

    return false;
}

bool SetDescriptor::hasValue(const string& key, int value)
{
    if (intFields.count(key))
        return (intFields[key] == value);

    return false;
}

bool SetDescriptor::hasValue(const string& key, float value)
{
    if (floatFields.count(key))
        return (floatFields[key] == value);

    return false;
}


void SetDescriptor::addValue(const string& key, const string& value)
{
    if (key.empty())
        throw UnknownKeyError("Unknown key " + key);

    if (!stringFields.count(key) && !floatFields.count(key) && !intFields.count(key))   // key not found
            throw UnknownKeyError("Unknown key " + key);

    if (!stringFields.count(key) && floatFields.count(key))    // key found in the float fields
        throw WrongTypeError("Set descriptor key <" + key + "> wants a float value but <" + value + "> is a string");

    if (!stringFields.count(key) && intFields.count(key))   // key found in the integer fields
        throw WrongTypeError("Set descriptor key <" + key + "> wants an integer value but <" + value + "> is a string");

    stringFields[key] = value;      // the old value is overwritten
}


void SetDescriptor::addValue(const string& key, int value)
{
    if (key.empty())
        throw UnknownKeyError("Unknown key " + key);

    if (!intFields.count(key) && !floatFields.count(key) && !stringFields.count(key))   // key not found
            throw UnknownKeyError("Unknown key " + key);

    if (!intFields.count(key) && floatFields.count(key))    // key found in the float fields
        throw WrongTypeError("Set descriptor key <" + key + "> wants a float value and not an integer one");

    if (!intFields.count(key) && stringFields.count(key))   // key found in the string fields
        throw WrongTypeError("Set descriptor key <" + key + "> wants a string value and not an integer one");

    intFields[key] = value;     // the old value is overwritten
}


void SetDescriptor::addValue(const string& key, float value)
{
    if (key.empty())
        throw UnknownKeyError("Unknown key " + key);

    if (!existKey(key))   // key not found
        throw UnknownKeyError("Unknown key " + key);

    if (!floatFields.count(key) && intFields.count(key))    // key found in the integer fields
        throw WrongTypeError("Set descriptor key '" + key + "' wants an integer value and not a float one");

    if (!floatFields.count(key) && stringFields.count(key))   // key found in the string fields
        throw WrongTypeError("Set descriptor key '" + key + "' wants a string value and not a float one");

    floatFields[key] = value;       // the old value is overwritten
}


/* Two SetDescriptors are equal if they have the same sequence number */

bool SetDescriptor::operator==(const SetDescriptor& other)
{
    return (sequenceNumber == other.getSequenceNumber());
}

bool SetDescriptor::operator!=(const SetDescriptor& other)
{
    return !(*this == other);
}

namespace Entity
{
    template<>
    bool SetDescriptor::addKey<string>(const string& key)
    {
        if (key.empty())
        {
            return false;
        }

        if (stringFields.count(key))
        {
            return true; // key already existing
        }

        auto response = stringFields.insert({key, string()});

        return response.second;
    }

    template<>
    bool SetDescriptor::addKey<int>(const string& key)
    {
        if (key.empty())
        {
            return false;
        }

        if (intFields.count(key))
        {
            return true;
        }

        auto response = intFields.insert({key, int()});

        return response.second;
    }

    template<>
    bool SetDescriptor::addKey<float>(const string& key)
    {
        if (key.empty())
        {
            return false;
        }

        if (floatFields.count(key))
        {
            return true;
        }

        auto response = floatFields.insert({key, float()});

        return response.second;
    }

    ostream& operator<<(ostream& os, const SetDescriptor& sd)
    {
        os << "SD\n\n";

        os << Protocol::Syntax::SN_SPECIAL_KEY << " " << sd.sequenceNumber << endl;

        for (const auto& it : sd.stringFields)
        {
            os << it.first << " " << it.second << endl ;
        }

        for (const auto& it : sd.intFields)
        {
            os << it.first << " " << it.second << endl;
        }

        for (const auto& it : sd.floatFields)
        {
            if (it.second == 0)
                os << it.first << " " << DEFAULT_SERIALIZABLE_FLOAT << endl;   // print "0.0", otherwise it would print "0" which would be considered an integer field

            else
                os << it.first << " " << it.second << endl;
        }

        return os;
    }
}
