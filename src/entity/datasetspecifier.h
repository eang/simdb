/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef DATA_SET_SPECIFIER_H
#define DATA_SET_SPECIFIER_H

#include <vector>

#include "dataitem.h"
#include "setdescriptor.h"

namespace Entity
{
    /**
    * @brief A DataSetSpecifier (DSS) is an object representing a single configuration file of the SimDB system.
    *
    * A DSS describes a SetDescriptor and the existing DataItems, through the content within the configuration file.
    * A DSS is identified by the name of its configuration file, excluding the .conf extension (if any).
    */
    class DataSetSpecifier
    {

    public:

        static const unsigned int MAX_ITEMS_NUMBER = 512;   /** Max DataItems number allowed. */

        explicit DataSetSpecifier(const std::string& n = "");

        void insertDataItem(const DataItem& d);
        SetDescriptor& getSetDescriptor();
        const SetDescriptor& getSetDescriptor() const;
        const std::vector<DataItem>& getDataItems() const;
        std::vector<DataItem>& getDataItems();
        std::string getName() const;

        bool operator==(const DataSetSpecifier& other);
        bool operator!=(const DataSetSpecifier& other);

    protected:

        std::string name;                           /** DSS name (i.e. the name of its configuration file). */
        SetDescriptor setDescriptor;                /** SetDescriptor defined within the configuration file. */
        std::vector<DataItem> dataItemsArray;       /** DataItems within the configuration file. */
    };
}


#endif
