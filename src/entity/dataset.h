/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef DATA_SET_H
#define DATA_SET_H

#include <string>

#include "datasetspecifier.h"

namespace Entity
{
    /**
    * @brief A Data Set (DS) is an instance of a DSS and is defined by a SetDescriptor and a set of DataItems.
    */

    class DataSet : public DataSetSpecifier
    {

    public:

        DataSet(const Entity::DataSetSpecifier& dss);

        void setTimestamp(const std::string& time);
        std::string getTimestamp() const;
        void setSequenceNumber(int sn);
        int getSequenceNumber() const;

        friend std::ostream& operator<<(std::ostream& os, const DataSet& ds);

    private:

        std::string timestamp;  /** Date and hour of the DataSet creation. */
    };
}


#endif
