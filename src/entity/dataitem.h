/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef DATA_ITEM_H
#define DATA_ITEM_H

#include <string>

namespace Entity
{
    /** DataItem possible tags */
    enum class Tag
    {
        Necessary,
        Unnecessary,
        Unspecified
    };

    /** DataItem possible groups */
    enum class Group
    {
        Input,
        Output,
        Unspecified
    };


    /**
    * @brief A DataItem (DI) is an object which represents a single file and is the basic unit of the SimDB system.
    *
    * A DataItem is described by a name, a tag and a group. There is also the name of the "instance" file provided by the client at the time of the DataSet creation, as well as the byte size of this file.
    * For convenience reasons this class holds also the base64-encoded size of the file and a pointer to the encoded block.
    */
    class DataItem
    {

    public:

        explicit DataItem(const std::string& n = "",
                          Tag t = Entity::Tag::Unspecified,
                          Group g = Entity::Group::Unspecified,
                          const std::string& f = "");
        ~DataItem();

        void setName(const std::string& n);
        void setTag(Tag t);
        void setGroup(Group g);
        void setFileName(const std::string& n);
        void setOriginalSize(size_t s);
        void setEncodedSize(size_t s);
        void setEncoding(char *enc);

        std::string getName() const;
        Tag getTag() const;
        Group getGroup() const;
        std::string getFileName() const;
        size_t getOriginalSize() const;
        size_t getEncodedSize() const;
        char *getEncoding() const;

        /** @param p Define whether the DataItem is present within the system. */
        void isPresent(bool p);

        /** @return true if the DataItem is present within the system (i.e. it's been instantiated), false otherwise. */
        bool isPresent() const;

        bool operator==(const DataItem& other) const;
        bool operator!=(const DataItem& other) const;

    private:
        std::string name;
        Tag tag;
        Group group;
        std::string fileName;   /** Name of the instance file for the DataItem in a new DataSet. */
        size_t originalSize;    /** Original byte size of the instance file. */
        size_t encodedSize;     /** Byte size of the file after the base64 encoding. */
        char *encoding;         /** Pointer to the base64-encoded  file. */
        bool present;           /** Boolean member used to simplify the UNNECESSARY DataItems tracking. */
    };

}


#endif
