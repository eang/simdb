/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef SET_DESCRIPTOR_H
#define SET_DESCRIPTOR_H

#include <map>
#include <stdexcept>
#include <string>

namespace Entity
{
    /**
    * @brief A SetDescriptor (SD) is a set of pairs <key, value>, where the key is always a string, while the value may be an integer, a float or a string.
    *
    * The SD is implemented by a map for each of the three possible combinations.
    * The SD has a Sequence Number (SN), which is a normal field within the DSS configuration file but in the resulting SD object is a class member.
    */
    class SetDescriptor
    {

    public:

        static const unsigned int MAX_FIELDS_NUMBER = 512;      /** Max fields number allowed. */

        SetDescriptor();

        void setSequenceNumber(int sn);
        int getSequenceNumber() const;

        /** @return SetDescriptor fields number, including the "extra" field for the sequence number. "*/
        int size() const;

        const std::map<std::string, std::string>& getStringFields() const;
        const std::map<std::string, int>& getIntFields() const;
        const std::map<std::string, float>& getFloatFields() const;

        /**
         * @param key The key of a certain field.
         * @return true if the SetDescriptor has a field with the key <key>, false otherwise.
         */
        bool existKey(const std::string& key);

       /*
        * Verify if the SetDescriptor has the given value under the given key.
        * @param key A key of a field.
        * @param value The field value to be verified.
        */
        bool hasValue(const std::string& key, const std::string& value);
        bool hasValue(const std::string& key, int value);
        bool hasValue(const std::string& key, float value);

       /*
        * Add the given value to the given key.
        * @param key A key of a certain SetDesriptor field.
        * @param value The value to add to the SetDescriptor.
        * @throw UnknownKeyError if the key is empty or is not in the SetDescriptor
        * @throw WrongTypeError if the key exists but it's owned by another SetDescriptor map.
        */
        void addValue(const std::string& key, const std::string& value);
        void addValue(const std::string& key, int value);
        void addValue(const std::string& key, float value);

       /**
        * Add a new key but without its value. The type of the value is the template parameter.
        * @param key The key of the new field. It can't be an empty string.
        * @return true if the insert operation succeeds, false otherwise.
        */
        template<typename T> bool addKey(const std::string& key);

        bool operator==(const SetDescriptor& other);
        bool operator!=(const SetDescriptor& other);

        friend std::ostream& operator<<(std::ostream &os, const SetDescriptor& sd);

    private:

        static const int INVALID_SEQUENCE_NUMBER = -1;              /** Default, not valid Sequence Number, to be replaced by a valid one. */

        int sequenceNumber;                                         /** SetDescriptor identifier. */
        std::map<std::string, std::string> stringFields;            /** SetDescriptor string fields. */
        std::map<std::string, int> intFields;                       /** SetDescriptor integer fields. */
        std::map<std::string, float> floatFields;                   /** SetDescriptor float fields. */
    };

    /** Exception thrown when a given key does not match any SetDescriptor field. */
    class UnknownKeyError : public std::runtime_error
    {
        public:
            UnknownKeyError(const std::string& msg) : std::runtime_error(msg) {}
    };

    /** Exception thrown when a given value for a given field key has a wrong type. */
    class WrongTypeError : public std::runtime_error
    {
        public:
            WrongTypeError(const std::string& msg) : std::runtime_error(msg) {}
    };


    const std::string DEFAULT_SERIALIZABLE_FLOAT = "0.0";
}

#endif
