/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include "dataitem.h"

using std::string;

using Entity::DataItem;
using Entity::Group;
using Entity::Tag;


DataItem::DataItem(const string &n, Tag t, Group g, const string &f) :
    name {n},
    tag {t},
    group {g},
    fileName {f},
    originalSize {0},
    encodedSize {0},
    encoding {nullptr},
    present {false}
{}

DataItem::~DataItem()
{
    encoding = nullptr;
}


void DataItem::setName(const string& n)
{
    name = n;
}


void DataItem::setTag(Tag t)
{
    tag = t;
}


void DataItem::setGroup(Group g)
{
    group = g;
}

void DataItem::setFileName(const string& n)
{
    fileName = n;
}

void DataItem::setOriginalSize(size_t s)
{
    originalSize = s;
}

void DataItem::setEncodedSize(size_t s)
{
    encodedSize = s;
}

void DataItem::setEncoding(char *enc)
{
    encoding = enc;
}


string DataItem::getName() const
{
    return name;
}


Tag DataItem::getTag() const
{
    return tag;
}


Group DataItem::getGroup() const
{
    return group;
}

string DataItem::getFileName() const
{
    return fileName;
}

size_t DataItem::getOriginalSize() const
{
    return originalSize;
}

size_t DataItem::getEncodedSize() const
{
    return encodedSize;
}

char *DataItem::getEncoding() const
{
    return encoding;
}

void DataItem::isPresent(bool p)
{
    present = p;
}

bool DataItem::isPresent() const
{
    return present;
}


/* Two DataItems are equal if they have the same name. */

bool DataItem::operator==(const DataItem& other) const
{
    return (name == other.getName());
}

bool DataItem::operator!=(const DataItem& other) const
{
    return !(*this == other);
}

