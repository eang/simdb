/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include "datasetspecifier.h"

using std::string;
using std::vector;

using Entity::DataItem;
using Entity::DataSetSpecifier;
using Entity::SetDescriptor;


DataSetSpecifier::DataSetSpecifier(const string& n) :
    name {n}
{}


void DataSetSpecifier::insertDataItem(const DataItem& d)
{
    dataItemsArray.push_back(d);
}

SetDescriptor& DataSetSpecifier::getSetDescriptor()
{
    return setDescriptor;
}

const SetDescriptor& DataSetSpecifier::getSetDescriptor() const
{
    return setDescriptor;
}

vector<DataItem>& DataSetSpecifier::getDataItems()
{
    return dataItemsArray;
}

const vector<DataItem>& DataSetSpecifier::getDataItems() const
{
    return dataItemsArray;
}


string DataSetSpecifier::getName() const
{
    return name;
}


bool DataSetSpecifier::operator==(const DataSetSpecifier& other)
{
    return (name == other.getName());
}

bool DataSetSpecifier::operator!=(const DataSetSpecifier& other)
{
    return !(*this == other);
}


