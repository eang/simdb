/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cerrno>
#include <cstdio>

#include "filesystem/filesystem.h"
#include "utils/utils.h"

using std::string;
using std::vector;

bool Filesystem::existFile(const string& path)
{
    struct stat fileInfo;

    return stat(path.c_str(), &fileInfo) == 0;
}

bool Filesystem::existDirectory(const string& path)
{
    struct stat dirInfo;

    if (stat(path.c_str(), &dirInfo) == 0)
        return S_ISDIR(dirInfo.st_mode);

    return false;
}

bool Filesystem::existDirectory(const string& parentDirectory, const string& name)
{
    string path = parentDirectory + "/" + name;
    struct stat dirInfo;

    if (stat(path.c_str(), &dirInfo) == 0)
        return S_ISDIR(dirInfo.st_mode);

    return false;
}

void Filesystem::listDirectory(const string& dirName, vector<string>& entries)
{
    string name;
    DIR *directory = opendir(dirName.c_str());

    if (directory)
    {
        auto nextEntry = readdir(directory);

        while (nextEntry)
        {
            name = nextEntry->d_name;

            if (name != "." && name != "..")
            {
                entries.push_back(name);
            }

            nextEntry = readdir(directory);
        }

        closedir(directory);
    }

    else
        throw Error(Utils::syscallError("opendir() on " + dirName, errno));

}

void Filesystem::createDirectory(const string& path)
{
    if (mkdir(path.c_str(), S_IRWXU) != 0)   // Read, write, execute only for the owner
        throw Error(Utils::syscallError("mkdir() on " + path, errno));
}

void Filesystem::createDirectory(const string& parentDirectory, const string& name)
{
    string path = parentDirectory + "/" + name;

    if (mkdir(path.c_str(), S_IRWXU) != 0)
        throw Error(Utils::syscallError("mkdir() on " + path, errno));
}

void Filesystem::removeDirectory(const string& path)
{
    if (!existDirectory(path))
        throw Error(path + " is not a directory");

    vector<string> content;
    listDirectory(path, content);

    string childPath;
    for (const auto& dir : content)
    {
        childPath = path + "/" + dir;

        if (existDirectory(childPath))   // is a directory, we remove it using recursion
            removeDirectory(childPath);

        else if (unlink(childPath.c_str()) != 0)    // is a file
            throw Error(Utils::syscallError("unlink() on " + childPath, errno));
    }

    /* Recursion base case: once that the directoy has been recursively emptied, we can finally remove it with the UNIX api */

    if (rmdir(path.c_str()) != 0)
        throw Error(Utils::syscallError("rmdir() on " + path, errno));
}

void Filesystem::renameDirectory(const string& path, const string& newPath)
{
    if (rename(path.c_str(), newPath.c_str()) != 0)
        throw Error(Utils::syscallError("rename() on " + path, errno));
}

long long Filesystem::getFileSize(const string& path)
{
    struct stat fileInfo;

    if (stat(path.c_str(), &fileInfo) != 0)
        throw Error(Utils::syscallError("stat() on " + path, errno));

    return fileInfo.st_size;
}

string Filesystem::getCurrentDirectory()
{
    char buffer[MAX_PATH_SIZE];

    char *cwdPath = getcwd(buffer, MAX_PATH_SIZE);

    if (cwdPath == nullptr)
        throw Error(Utils::syscallError("getcwd()", errno));

    return string(buffer);
}

bool Filesystem::isAbsolutePath(const string& path)
{
    if (path.empty())   // an absolute path cannot be empty
        return false;

    return '/' == path[0];  // on UNIX a path is absolute if it starts with a slash
}


