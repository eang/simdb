/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <stdexcept>
#include <string>
#include <vector>

/** @namespace filesystem A namespace with filesystem utility functions. */
namespace Filesystem
{
    static const int MAX_PATH_SIZE = 2048;   /** Max size allowed for an absolute path. */

   /**
    * @param path The path of a certain file.
    * @return true if the file exists, false if it doesn't or if there is an error.
    */
    bool existFile(const std::string& path);

   /**
    * @param path The path of a certain directory.
    * @return true if the directory exists, false if it doesn't or if there is an error.
    */
    bool existDirectory(const std::string& path);

   /**
    * @param parentDirectory The path of a certain directory.
    * @param name The name of a certain directory.
    * @return true if <name> exists as child of <parentDirectory>, false if it doesn't or if there is an error.
    */
    bool existDirectory(const std::string& parentDirectory, const std::string& name);

   /** List a given directory.
    * @param path The path of a certain directory.
    * @param entries A string vector to store the directory content.
    * @throw FileSystemError if the operation has failed.
    */
    void listDirectory(const std::string& path, std::vector<std::string>& entries);

   /** Create a directory in the given absolute path.
    * @param path An absolute directory path.
    * @throw FileSystemError if the operation has failed.
    */
    void createDirectory(const std::string& path);

   /** Create a directory in the given parent directory.
    * @param parentDirectory The absolue path of a parent directory.
    * @param name A name for the new directory.
    * @throw FileSystemError if the operation has failed.
    */
    void createDirectory(const std::string& parentDirectory, const std::string& name);

   /** Remove the given absolute directory path.
    * @param path An absolute directory path.
    * @throw FileSystemError if the operation has failed.
    */
    void removeDirectory(const std::string& path);

   /** Rename the given absolute directory path.
    * @param newPath The new name for the directory.
    * @throw FileSystemError if the operation has failed.
    */
    void renameDirectory(const std::string& path, const std::string& newPath);

   /**
    * @param path An absolute file path.
    * @return The size of the given file (a 64 bit integer).
    * @throw FileSystemError if the operation has failed.
    */
    long long getFileSize(const std::string& path);

   /**
    * @return String with the Current Working Director (CWD) of the calling process, as an absolute path.
    * @throw FileSystemError if the operation has failed.
    */
    std::string getCurrentDirectory();

   /**
    * @param path A path of a file or a directory.
    * @return true if the given path is an absolute path, false otherwise.
    */
    bool isAbsolutePath(const std::string& path);

   /**
    * Exception thrown when a filesystem operation has failed. We use an exception because usually the name of a file or a directory is an external input and then is better to forward the error to the "level" which is in charge to handle the input.
    */
    class Error : public std::runtime_error
    {
        public:
            Error(const std::string& msg) : std::runtime_error(msg) {}
    };

}


#endif
