/*
    Copyright (c) 2014 by Elvis Angelaccio

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#if (defined(_WIN32) || defined(__WIN32__) || defined(WIN32))

/*
 * Macro required by GetFileSizeEx()
 * source: http://msdn.microsoft.com/en-us/library/aa383745(v=vs.85).aspx#setting_winver_or__win32_winnt
 */
#define WINVER 0x0500

#include <windows.h>

#endif

#include "filesystem/filesystem.h"
#include "utils/utils.h"

using namespace std;

bool Filesystem::existFile(const string& path)
{
    auto attributes = GetFileAttributes(path.c_str());

    return (attributes != INVALID_FILE_ATTRIBUTES  && !(attributes & FILE_ATTRIBUTE_DIRECTORY));  // i.e. it exists but it's not a directory
}

bool Filesystem::existDirectory(const string& path)
{
    auto attributes = GetFileAttributes(path.c_str());

    if (attributes == INVALID_FILE_ATTRIBUTES)
        return false;

    return attributes & FILE_ATTRIBUTE_DIRECTORY;
}


bool Filesystem::existDirectory(const string& parentDirectory, const string& name)
{
    string path = parentDirectory + "/" + name;

    auto attributes = GetFileAttributes(path.c_str());

    if (attributes == INVALID_FILE_ATTRIBUTES)
        return false;

    return attributes & FILE_ATTRIBUTE_DIRECTORY;
}


void Filesystem::listDirectory(const string& path, vector<string>& entries)
{
    WIN32_FIND_DATA fileData;
    auto searchHandle = FindFirstFile((path + "/*").c_str(), &fileData);     // Windows requires the wildcard *

    if (searchHandle == INVALID_HANDLE_VALUE)
    {
        throw Error(Utils::syscallError("FindFirstFile()", GetLastError()));
    }

    string name;

    do
    {
        name = fileData.cFileName;

        if (name != "." && name != "..")
        {
            entries.push_back(fileData.cFileName);
        }
    }
    while (FindNextFile(searchHandle, &fileData));

    FindClose(searchHandle);

}

void Filesystem::createDirectory(const string& path)
{
    if (!CreateDirectory(path.c_str(), nullptr))
    {
        throw Error(Utils::syscallError("CreateDirectory() on " + path, GetLastError()));
    }
}


void Filesystem::createDirectory(const string& parentDirectory, const string& name)
{
    string path = parentDirectory + "/" + name;

    if (!CreateDirectory(path.c_str(), nullptr))
    {
        throw Error(Utils::syscallError("CreateDirectory() on " + path, GetLastError()));
    }
}

void Filesystem::removeDirectory(const string& path)
{
    if (!existDirectory(path))
        throw Error(path + " is not a directory");

    vector<string> content;
    listDirectory(path, content);

    string childPath;
    for (const auto& dir : content)
    {
        childPath = path + "/" + dir;

        if (existDirectory(childPath))   // is a directory we remove it using recursion
            removeDirectory(childPath);

        else if (!DeleteFile(childPath.c_str()))    // is a file
        {
            throw Error(Utils::syscallError("DeleteFile() on " + childPath, GetLastError()));
        }
    }

    /* Recursion base case: once that the directoy has been recursively emptied, we can finally remove it with the Win32 api */

    if (!RemoveDirectory(path.c_str()))
    {
        throw Error(Utils::syscallError("RemoveDirectory() on " + path, GetLastError()));
    }
}

void Filesystem::renameDirectory(const string& path, const string& newPath)
{
    if (!MoveFile(path.c_str(), newPath.c_str()))
    {
        throw Error(Utils::syscallError("MoveFile() on " + path, GetLastError()));
    }
}

long long Filesystem::getFileSize(const string& path)
{
    auto file = CreateFile(path.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

    if (file == INVALID_HANDLE_VALUE)
    {
        throw Error(Utils::syscallError("CreateFile on opening " + path, GetLastError()));
    }

    LARGE_INTEGER size;
    if (!GetFileSizeEx(file, &size))
    {
        throw Error(Utils::syscallError("GetFileSizeEx() on " + path, GetLastError()));
    }

    CloseHandle(file);
    return size.QuadPart;
}

string Filesystem::getCurrentDirectory()
{
    char buffer[MAX_PATH_SIZE];
    auto pathSize = GetCurrentDirectory(MAX_PATH_SIZE, buffer);

    if (0 == pathSize)
    {
        throw Error(Utils::syscallError("GetCurrentDirectory()", GetLastError()));
    }

    return string(buffer);
}

bool Filesystem::isAbsolutePath(const std::string& path)
{
    if (path.length() < 3)  // On Windows an absolute path has at least 3 characters: a "driver" letter, the ':' char and then a slash or a backslash
        return false;

    if (isalpha(path[0]) && ':' == path[1] && '\\' == path[2])  // a path like "C:\foo.txt" is absolute
        return true;

    if (isalpha(path[0]) && ':' == path[1] && '/' == path[2])  // a path like "C:/foo.txt" is absolute
        return true;

    return false;
}

