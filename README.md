SimDB
======

SimDB is a client/server system designed to handle the input/output data of a generic application.
The system defines an HTTP-like network protocol for the client/server communication.

For more informations check the official page: http://aelog.org/simdb 
